import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//Controllers
import 'package:ondunova_app/Navigation/tabBar/bottomNavigationBarController.dart';
import 'package:ondunova_app/utils/materialColorFromHex.dart';
//Components

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
          [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatefulWidget {
/*   static final _myTabbedPageKey =
      new GlobalKey<_BottomNavigationBarControllerState>(); */

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: createMaterialColor(Color(0xFFb7ce00)),
        ),
        home: new BottomNavigationBarController(
            /* key: MyApp._myTabbedPageKey, */
            ));
  }
}
