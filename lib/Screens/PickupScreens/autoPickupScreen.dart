import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'dart:ui';
import 'package:ondunova_app/Class/apiResponseClass.dart';
import 'package:ondunova_app/Components/generalButton.dart';
import 'package:swipedetector/swipedetector.dart';
//Api
import 'package:ondunova_app/Api/apiProvider.dart';
//Class
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Components/alertDialog.dart';
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';
import 'package:ondunova_app/Screens/PickupScreens/pickupScreen.dart';
//Screens
import 'package:ondunova_app/Screens/PickupScreens/zonePickupScreen.dart';
//Styles
import 'package:ondunova_app/Styles/Screens/PickupScreens/autoPickupScreenStyles.dart';
//Utils
import 'package:ondunova_app/utils/materialColorFromHex.dart';
import 'package:ondunova_app/utils/ordersActions.dart';
//Components
import 'package:ondunova_app/Components/longButton.dart';

class AutoPickupScreen extends StatefulWidget {
  final Order order;
  final void Function(int) changeTab;
  final bool isUpdate;

  const AutoPickupScreen({Key key, this.changeTab, this.order, this.isUpdate})
      : super(key: key);

  @override
  _AutoPickupScreenState createState() => new _AutoPickupScreenState();
}

class _AutoPickupScreenState extends State<AutoPickupScreen> {
  Future<List<Order>> _futureOrders;

  ApiProvider _apiProvider = ApiProvider();
  OrderActions _orderActions = OrderActions();

  ApiResponse ordersResponse;
  ApiResponse updateOrderStatusResponse;
  List<Order> listOrders;
  bool isLoading = true;

  Timer timer;

  @override
  void initState() {
    super.initState();
    if (widget.isUpdate) {
      getOrders();
      SchedulerBinding.instance.addPostFrameCallback((_) {
        goToManualPickup(widget.order, false, false);
      });
    } else {
      timer = Timer.periodic(Duration(seconds: 15), (Timer t) => getOrders());
      getOrders();
    }
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  getOrders() async {
    setState(() {
      isLoading = true;
    });

    ordersResponse = await _apiProvider.getOrders();
    if (ordersResponse.success) {
      List<Order> orders = _orderActions.getOrdersFromResponse(ordersResponse);
      List<Order> filteredOrders =
          _orderActions.filterByOrderStatus(orders, OrderStatus.EnRecogida);
      setState(() {
        isLoading = false;
        listOrders = filteredOrders;
      });
    }
  }

  /* setOrders(List<Order> orderList) {
    setState(() {
      listOrders = orderList;
    });
    return orderList;
  } */

  pickupOrder(Order order) async {
    setState(() {
      isLoading = true;
    });
    var updateOrderStatusResponse =
        await _apiProvider.updateOrderStatusById(order.id);
    if (updateOrderStatusResponse.success) {
      Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ZonePickupScreen(order: order)))
          .then((value) => getOrders());
    } else {
      setState(() {
        isLoading = false;
      });
      _showDialog(order);
    }
  }

  goToManualPickup(Order order, bool isError, bool isScan) {
    if (isError) {
      if (isScan) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PickupScreen(
                    order: order,
                    isUpdate: true,
                    isError: isError,
                    isScan: isScan))).then((value) => getOrders());
      } else {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PickupScreen(
                    order: order,
                    isUpdate: true,
                    isError: isError,
                    isScan: isScan))).then((value) => getOrders());
      }
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PickupScreen(
                  order: order,
                  isUpdate: widget.isUpdate,
                  isError: false,
                  isScan: false))).then((value) => getOrders());
    }
  }

  void _showDialog(Order order) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomAlertDialog(
            title: "Error al recollir el palet nº " +
                order.numPalet.toString() +
                " amb número de OF " +
                order.numOf,
            subtitle: "vol tornar a intentar ?",
            onValidationPressed: () async {
              Navigator.of(context).pop();
              pickupOrder(order);
            },
            onRejectPressed: () {
              Navigator.of(context).pop();
            },
            cancelText: 'No',
            validateText: 'Si',
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: createMaterialColor(Color(0xFF00644b)),
          centerTitle: true,
          title: Text(
            'Recollida',
            style: appBarTextStyle,
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: GestureDetector(
                  onTap: () {
                    getOrders();
                  },
                  child:
                      Icon(Icons.refresh, size: 30.0, color: Color(0xFFb7ce00)),
                )),
            Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: IconButton(
                    icon: Icon(Icons.dialpad),
                    color: Color(0xFFb7ce00),
                    onPressed: () {
                      goToManualPickup(null, false, false);
                    }))
          ],
        ),
        body: SwipeDetector(
            onSwipeLeft: () {
              widget.changeTab(2);
            },
            onSwipeRight: () {
              widget.changeTab(1);
            },
            child: (isLoading)
                ? Center(
                    child: SizedBox(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator()))
                : (!ordersResponse.success)
                    ? Center(
                        child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: iconSize,
                          ),
                          SizedBox(height: 20),
                          Text(ordersResponse.message),
                          SizedBox(height: 20),
                          GeneralButton(
                              onPressed: () async {
                                getOrders()();
                              },
                              color: Color(0xFF00644b),
                              buttonText: 'Tornar a provar'),
                        ],
                      ))
                    : (listOrders.length == 0)
                        ? Container(
                            padding: EdgeInsets.only(top: 5, bottom: 5),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.check_circle_outline_outlined,
                                          color: Color(0xFFb7ce00),
                                          size: 60.0,
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          'No hi ha més ordres a recollir',
                                          style: messageTextStyle,
                                        ),
                                      ]),
                                ]))
                        : Container(
                            /* height: screenSize.height,
                width: screenSize.width, */
                            padding: EdgeInsets.only(top: 5, bottom: 5),
                            child: ListView.builder(
                                itemCount: listOrders.length,
                                itemBuilder: (context, index) {
                                  var pickupDate = "";
                                  if (listOrders[index].pickupDate != null) {
                                    pickupDate =
                                        DateFormat('yyyy/MM/dd HH:mm:ss')
                                            .format(DateTime.parse(
                                                listOrders[index].pickupDate));
                                  }

                                  if (listOrders[index]
                                      .numOf
                                      .contains("ERROR")) {
                                    return Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 15),
                                        decoration: BoxDecoration(
                                            color:
                                                Color.fromRGBO(255, 87, 51, 1)),
                                        child: Card(
                                            elevation: 0,
                                            color: Color(0xFFe6e6e6),
                                            shape: BeveledRectangleBorder(),
                                            child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  ListTile(
                                                    tileColor: Color.fromRGBO(
                                                        255, 87, 51, 1),
                                                    leading: Text(
                                                        listOrders[index].numOf,
                                                        style:
                                                            numOfItemTextStyle),
                                                  ),
                                                  Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        SizedBox(height: 20.0),
                                                        Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: [
                                                              LongButton(
                                                                  onPressed:
                                                                      () async {
                                                                    goToManualPickup(
                                                                        listOrders[
                                                                            index],
                                                                        true,
                                                                        false)();
                                                                  },
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          255,
                                                                          87,
                                                                          51,
                                                                          1),
                                                                  buttonText:
                                                                      'Introducir manualmente'),
                                                              SizedBox(
                                                                width: 20,
                                                              ),
                                                              LongButton(
                                                                  onPressed:
                                                                      () async {
                                                                    goToManualPickup(
                                                                        listOrders[
                                                                            index],
                                                                        true,
                                                                        true)();
                                                                  },
                                                                  color: Color
                                                                      .fromRGBO(
                                                                          255,
                                                                          87,
                                                                          51,
                                                                          1),
                                                                  buttonText:
                                                                      'Escanear etiqueta'),
                                                            ]),
                                                        SizedBox(
                                                          height: 40.0,
                                                        ),
                                                      ])
                                                ])));
                                  } else {
                                    return Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 15),
                                        decoration: BoxDecoration(
                                            color: Color(0xFF00644b)),
                                        child: Card(
                                            elevation: 2,
                                            color: Color(0xFFe6e6e6),
                                            shape: BeveledRectangleBorder(),
                                            child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  ListTile(
                                                    tileColor:
                                                        Color(0xFF00644b),
                                                    leading: Text(
                                                        listOrders[index].numOf,
                                                        style:
                                                            numOfItemTextStyle),
                                                    title: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Text(pickupDate,
                                                              style:
                                                                  subTitleTextStyle),
                                                          SizedBox(
                                                            width: 50.0,
                                                          ),
                                                          Text(
                                                              "segueix en --> " +
                                                                  listOrders[
                                                                          index]
                                                                      .machineName,
                                                              style:
                                                                  titleTextStyle),
                                                        ]),
                                                  ),
                                                  SizedBox(height: 20.0),
                                                  Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        Text(
                                                          'Número de Palet: ${listOrders[index].numPalet}',
                                                          style:
                                                              detailOrderTextStyle,
                                                        ),
                                                        (listOrders[index]
                                                                    .quantity !=
                                                                null)
                                                            ? Text(
                                                                'Quantitat: ${listOrders[index].quantity.toString().trim()}',
                                                                style:
                                                                    detailOrderTextStyle)
                                                            : Text(
                                                                'Quantitat: ',
                                                                style:
                                                                    detailOrderTextStyle),
                                                        Text(
                                                          'Total Palets: ${listOrders[index].totalPalets}',
                                                          style:
                                                              detailOrderTextStyle,
                                                        ),
                                                      ]),
                                                  SizedBox(height: 20.0),
                                                  Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        (listOrders[index]
                                                                    .origin !=
                                                                null)
                                                            ? Text(
                                                                'Nom del client: ${listOrders[index].clientName.trim()}',
                                                                style:
                                                                    detailOrderTextStyle,
                                                              )
                                                            : Text(
                                                                'Nom del client: ',
                                                                style:
                                                                    detailOrderTextStyle,
                                                              ),
                                                        (listOrders[index]
                                                                    .origin !=
                                                                null)
                                                            ? Text(
                                                                'Origen: ${listOrders[index].origin.trim()}',
                                                                style:
                                                                    detailOrderTextStyle,
                                                              )
                                                            : Text(
                                                                'Origen: ',
                                                                style:
                                                                    detailOrderTextStyle,
                                                              ),
                                                      ]),
                                                  SizedBox(height: 30.0),
                                                  Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        PickupButton(
                                                            onPressed: () {
                                                          pickupOrder(
                                                              listOrders[
                                                                  index]);
                                                        })
                                                      ]),
                                                  SizedBox(
                                                    height: 20.0,
                                                  ),
                                                ])));
                                  }
                                }))));
  }
}

class PickupButton extends StatelessWidget {
  PickupButton({this.onPressed});

  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Text("Recollir", style: pickupButtonTextStyle),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: 200,
        height: 60,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
      fillColor: createMaterialColor(Color(0xFF00644b)),
    );
  }
}
