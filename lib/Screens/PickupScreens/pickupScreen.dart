import 'package:flutter/foundation.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:async';
//Api
import 'package:ondunova_app/Api/apiProvider.dart';
import 'package:ondunova_app/Class/apiResponseClass.dart';
//Class
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Class/positionSuggestionClass.dart';
//Components
import 'package:ondunova_app/Components/numpad.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:ondunova_app/Components/alertDialog.dart';
//Constants
import 'package:ondunova_app/Constants/Enums/enums.dart';
import 'package:ondunova_app/Constants/Enums/storeZoneEnum.dart';
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';
//Styles
import 'package:ondunova_app/Styles/Screens/PickupScreens/pickupScreenStyles.dart';
//Utils
import 'package:ondunova_app/utils/materialColorFromHex.dart';
import 'package:ondunova_app/utils/ordersActions.dart';
import 'package:ondunova_app/utils/stringFromEnum.dart';

class PickupScreen extends StatefulWidget {
  final Order order;
  final void Function(int) changeTab;
  final bool isUpdate;
  final bool isError;
  final bool isScan;
  PickupScreen(
      {Key key,
      this.order,
      this.isUpdate,
      this.isError,
      this.isScan,
      this.changeTab})
      : super(key: key);

  @override
  _PickupScreenState createState() => _PickupScreenState();
}

class _PickupScreenState extends State<PickupScreen> {
  Future<ApiResponse> _futureOrderExist;
  Future<ApiResponse> _futureOrderPickup;
  Future<ApiResponse> _futureZoneSuggestion;

  ApiProvider _apiProvider = ApiProvider();
  StringFromEnum stringFromEnum = StringFromEnum();
  OrderActions _orderActions = OrderActions();

  PickupInputType inputType = PickupInputType.NumOf;
  StoreZoneType zoneType = StoreZoneType.Zone1;

  Order currentOrder;

  bool orderExist = false;
  bool orderPickupSuccess = false;

  bool numPaletIsValidated = true;
  bool quantityIsValidated = true;

  //InputValues
  int orderId = 0;
  String numOrder = "";
  String numPalet = "";
  String quantity = "";
  String storeZone = "";
  String currentValue = "";

  Order orderPickup = new Order();

  void initState() {
    super.initState();

    if (widget.isScan) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        _scanCode();
      });
    }

    if (widget.order != null) {
      StoreZoneType currentStoreZone =
          stringFromEnum.getStoreZoneValue(widget.order.storePosition);

      if (widget.order.numOf.contains("ERROR")) {
        setState(() {
          currentOrder = widget.order;
          orderId = widget.order.id;
          storeZone = stringFromEnum.getStringFromZoneType(StoreZoneType.Zone1);
          currentValue = numOrder;
        });
      } else {
        setState(() {
          currentOrder = widget.order;
          orderId = widget.order.id;
          numOrder = widget.order.numOf;
          numPalet = widget.order.numPalet.toString();
          quantity = widget.order.quantity.toString();
          storeZone = widget.order.storePosition;
          currentValue = numOrder;
          zoneType = currentStoreZone;
        });
      }
      validateOrder();
    } else {
      setState(() {
        numOrder = "";
        numPalet = "";
        quantity = "";
        currentValue = numOrder;
        storeZone = stringFromEnum.getStringFromZoneType(StoreZoneType.Zone1);
      });
    }
  }

  setInputValue(String value) async {
    switch (inputType) {
      case PickupInputType.NumOf:
        setState(() {
          numOrder = value;
          currentValue = value;
        });
        validateOrder();
        break;

      case PickupInputType.NumPalet:
        return setState(() {
          numPalet = value;
          currentValue = value;
          numPaletIsValidated = true;
        });
        break;
      case PickupInputType.Quantity:
        return setState(() {
          quantity = value;
          currentValue = value;
          quantityIsValidated = true;
        });
        break;
    }
  }

  //Api calls
  sendOrderPickup() async {
    await validateInputs();

    if (numPaletIsValidated && quantityIsValidated) {
      var orderPickup;
      if (widget.isUpdate) {
        if (widget.isError) {
          orderPickup = new Order(
            id: orderId,
            numOf: numOrder,
            numPalet: int.parse(numPalet),
            quantity: int.parse(quantity),
            storePosition: storeZone,
            orderStatus: OrderStatus.EnAlmacen.status,
            totalPalets: currentOrder.totalPalets,
            clientId: currentOrder.clientId,
            clientName: currentOrder.clientName,
            machineId: currentOrder.machineId,
            machineName: currentOrder.machineName,
            origin: currentOrder.origin,
          );
        } else {
          orderPickup = new Order(
            id: orderId,
            numOf: numOrder,
            numPalet: int.parse(numPalet),
            quantity: int.parse(quantity),
            storePosition: storeZone,
            orderStatus: currentOrder.orderStatus,
            totalPalets: currentOrder.totalPalets,
            clientId: currentOrder.clientId,
            clientName: currentOrder.clientName,
            machineId: currentOrder.machineId,
            machineName: currentOrder.machineName,
            origin: currentOrder.origin,
          );
        }

        setState(() {
          orderPickup = orderPickup;
          _futureOrderPickup = _apiProvider
              .updateOrderPickup(orderPickup)
              .then((response) => setOrderPickupSuccess(response));
        });
      } else {
        var orderPickup = new Order(
          numOf: numOrder,
          numPalet: int.parse(numPalet),
          quantity: int.parse(quantity),
          storePosition: storeZone,
          totalPalets: currentOrder.totalPalets,
          clientId: currentOrder.clientId,
          clientName: currentOrder.clientName,
          machineId: currentOrder.machineId,
          machineName: currentOrder.machineName,
          origin: currentOrder.origin,
        );

        setState(() {
          orderPickup = orderPickup;
          _futureOrderPickup = _apiProvider
              .newOrderPickup(orderPickup)
              .then((response) => setOrderPickupSuccess(response));
        });
      }
    }
  }

  validateOrder() {
    if (numOrder != "") {
      setState(() {
        _futureOrderExist = _apiProvider
            .validateOrder(numOrder, "")
            .then((response) => updateVisibiity(response));
      });
    }
  }

  setPosition(positionSuggestionData) {
    StoreZoneType currentStoreZone;

    Iterable l = positionSuggestionData;
    List<PositionSuggestion> positionSuggestion = List<PositionSuggestion>.from(
        l.map((model) => PositionSuggestion.fromJson(model)));
    String storePosition = positionSuggestion[0].storePosition;

    if (storePosition != null) {
      currentStoreZone = stringFromEnum.getStoreZoneValue(storePosition);
      setState(() {
        zoneType = currentStoreZone;
        storeZone = stringFromEnum.getStringFromZoneType(currentStoreZone);
      });
    } else {
      currentStoreZone =
          stringFromEnum.getStoreZoneValue(widget.order.storePosition);
      setState(() {
        zoneType = currentStoreZone;
        storeZone = stringFromEnum.getStringFromZoneType(currentStoreZone);
      });
    }
  } //287324 287859

  updateVisibiity(ApiResponse response) {
    Order currentOrderData =
        _orderActions.getValidateOrderFromResponse(response);
    if (currentOrderData != null && widget.isUpdate && !widget.isError) {
      setState(() {
        orderExist = true;
      });
    } else if (currentOrderData != null && widget.isError) {
      setState(() {
        currentOrder = currentOrderData;
        orderExist = true;
        _futureZoneSuggestion = _apiProvider
            .getPositionSuggestion(currentOrderData.numOf)
            .then((zone) => setPosition(zone.data));
      });
    } else if (currentOrderData != null) {
      setState(() {
        currentOrder = currentOrderData;
        orderExist = true;
        _futureZoneSuggestion = _apiProvider
            .getPositionSuggestion(currentOrderData.numOf)
            .then((zone) => setPosition(zone.data));
      });
    } else {
      setState(() {
        orderExist = false;
      });
    }
    return response;
  }

  setOrderPickupSuccess(response) {
    setState(() {
      orderPickupSuccess = response.success;
    });

    if (widget.isError && response.success) {
      goBack();
    }

    return response;
  }

  validateInputs() {
    bool paletValidated = false;
    bool quantityValidated = false;
    if (numPalet.length > 0) {
      paletValidated = true;
    }

    if (quantity.length > 0) {
      quantityValidated = true;
    }

    setState(() {
      numPaletIsValidated = paletValidated;
      quantityIsValidated = quantityValidated;
    });
  }

  goBack() {
    Navigator.pop(context);
  }

  newOrderPickup() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomAlertDialog(
            title: "Es creàra una nova entrada",
            subtitle: "vol conservar les dades de l'entrada anterior?",
            onValidationPressed: () {
              resetOnlyStates();
              Navigator.of(context).pop();
            },
            onRejectPressed: () {
              resetData();
              Navigator.of(context).pop();
            },
            cancelText: 'No',
            validateText: 'Si',
          );
        });
  }

  resetOnlyStates() {
    setState(() {
      _futureOrderPickup = null;
      orderId = 0;
      orderExist = true;
      orderPickupSuccess = false;
      orderPickup = new Order();
    });
  }

  resetData() {
    setState(() {
      _futureOrderExist = null;
      _futureOrderPickup = null;
      orderId = 0;
      numOrder = "";
      numPalet = "";
      quantity = "";
      storeZone = stringFromEnum.getStringFromZoneType(StoreZoneType.Zone1);
      currentValue = "";
      orderExist = false;
      orderPickupSuccess = false;
      inputType = PickupInputType.NumOf;
      zoneType = StoreZoneType.Zone1;
      orderPickup = new Order();
    });
  }

  Future<void> _scanCode() async {
    var options = ScanOptions(
      //set the options
      autoEnableFlash: true,
      android: AndroidOptions(useAutoFocus: true),
    );

    var barcode = await BarcodeScanner.scan(options: options);
    if (barcode.rawContent.length > 0) {
      var infoTrim = barcode.rawContent.replaceAll(" ", '').replaceAll("R", "");
      var infoSplit = infoTrim.split("#");
      Order orderScan = new Order(
          numOf: infoSplit[0],
          numPalet: int.parse(infoSplit[1]),
          quantity: int.parse(infoSplit[3]));

      setState(() {
        numOrder = orderScan.numOf;
        numPalet = orderScan.numPalet.toString();
        quantity = orderScan.quantity.toString();
        currentValue = numOrder;
      });

      validateOrder();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: createMaterialColor(Color(0xFF00644b)),
        centerTitle: true,
        title: Text('Entrada Magatzem', style: appBarTextStyle),
        iconTheme: IconThemeData(
          color: Color(0xFFb7ce00),
        ),
      ),
      body: GestureDetector(
          onPanUpdate: (DragUpdateDetails details) {
            if (details.delta.dx > 0) {
              widget.changeTab(0);
            } else {
              widget.changeTab(2);
            }
          },
          child: Center(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Numpad(
                setInputValue: (String value) {
                  setInputValue(value);
                },
                currentValue: currentValue,
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: formPadding,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextInput(
                            inputTitle: "Número de OF",
                            inputValue: numOrder,
                            inputSelected: inputType,
                            inputType: PickupInputType.NumOf,
                            onPressed: () {
                              setState(() {
                                inputType = PickupInputType.NumOf;
                                currentValue = numOrder;
                              });
                            },
                            validated: true,
                          ),
                          SizedBox(width: widthSizedBoxValidation),
                          FutureBuilder<ApiResponse>(
                              future: _futureOrderExist,
                              builder: (context, snapshot) {
                                List<Widget> children;
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  children = <Widget>[
                                    CircularProgressIndicator(),
                                  ];
                                } else {
                                  if (numOrder == "") {
                                    children = <Widget>[];
                                  } /* else if (snapshot.hasError) {
                                    children = <Widget>[
                                      Icon(
                                        Icons.check_circle_outline,
                                        color: Color(0xFFb7ce00),
                                        size: iconSize,
                                      ),
                                    ];
                                  } */
                                  else if (snapshot.hasData) {
                                    if (snapshot.data.success) {
                                      children = <Widget>[
                                        Icon(
                                          Icons.check_circle_outline,
                                          color: Color(0xFFb7ce00),
                                          size: iconSize,
                                        ),
                                      ];
                                    } else {
                                      children = <Widget>[
                                        Icon(
                                          Icons.error_outline,
                                          color: Colors.red,
                                          size: iconSize,
                                        )
                                      ];
                                    }
                                  } else {
                                    children = <Widget>[
                                      Icon(
                                        Icons.error_outline,
                                        color: Colors.red,
                                        size: iconSize,
                                      )
                                    ];
                                  }
                                }

                                return Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: children,
                                );
                              })
                        ],
                      ),
                      SizedBox(
                        height: generalHeightSizedBox,
                      ),
                      Visibility(
                          visible: orderExist,
                          maintainSize: true,
                          maintainAnimation: true,
                          maintainState: true,
                          maintainInteractivity: true,
                          child: Column(children: <Widget>[
                            SizedBox(
                              height: generalHeightSizedBox,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                TextInput(
                                  inputTitle: "Número de palet",
                                  inputValue: numPalet,
                                  inputSelected: inputType,
                                  inputType: PickupInputType.NumPalet,
                                  onPressed: () {
                                    setState(() {
                                      inputType = PickupInputType.NumPalet;
                                      currentValue = numPalet;
                                      numPaletIsValidated = true;
                                    });
                                  },
                                  validated: numPaletIsValidated,
                                ),
                                SizedBox(
                                  width: inputWidthSizedBox,
                                ),
                                TextInput(
                                  inputTitle: "Quantitat",
                                  inputValue: quantity,
                                  inputSelected: inputType,
                                  inputType: PickupInputType.Quantity,
                                  onPressed: () {
                                    setState(() {
                                      inputType = PickupInputType.Quantity;
                                      currentValue = quantity;
                                      quantityIsValidated = true;
                                    });
                                  },
                                  validated: quantityIsValidated,
                                ),
                              ],
                            ),
                            Padding(
                              padding: titleStorePadding,
                              child: Text("Zona Almacen",
                                  style: titleStoreTextStyle),
                            ),
                            FutureBuilder<ApiResponse>(
                                future: _futureZoneSuggestion,
                                builder: (context, snapshot) {
                                  List<Widget> children;
                                  if (snapshot.connectionState ==
                                      ConnectionState.waiting) {
                                    children = <Widget>[
                                      CircularProgressIndicator(),
                                    ];
                                  } else {
                                    children = <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          StoreZone(
                                            onPressed: () {
                                              setState(() {
                                                storeZone = stringFromEnum
                                                    .getStringFromZoneType(
                                                        StoreZoneType.Zone1);
                                                zoneType = StoreZoneType.Zone1;
                                              });
                                            },
                                            zone: stringFromEnum
                                                .getStringFromZoneType(
                                                    StoreZoneType.Zone1),
                                            zoneSelected: zoneType,
                                            zoneType: StoreZoneType.Zone1,
                                          ),
                                          SizedBox(
                                              width: storeZoneSizedBoxWidth),
                                          StoreZone(
                                            onPressed: () {
                                              setState(() {
                                                storeZone = stringFromEnum
                                                    .getStringFromZoneType(
                                                        StoreZoneType.Zone2);
                                                zoneType = StoreZoneType.Zone2;
                                              });
                                            },
                                            zone: stringFromEnum
                                                .getStringFromZoneType(
                                                    StoreZoneType.Zone2),
                                            zoneSelected: zoneType,
                                            zoneType: StoreZoneType.Zone2,
                                          ),
                                          SizedBox(
                                              width: storeZoneSizedBoxWidth),
                                          StoreZone(
                                            onPressed: () {
                                              setState(() {
                                                storeZone = stringFromEnum
                                                    .getStringFromZoneType(
                                                        StoreZoneType.Zone3);
                                                zoneType = StoreZoneType.Zone3;
                                              });
                                            },
                                            zone: stringFromEnum
                                                .getStringFromZoneType(
                                                    StoreZoneType.Zone3),
                                            zoneSelected: zoneType,
                                            zoneType: StoreZoneType.Zone3,
                                          ),
                                          SizedBox(
                                              width: storeZoneSizedBoxWidth),
                                          StoreZone(
                                            onPressed: () {
                                              setState(() {
                                                storeZone = stringFromEnum
                                                    .getStringFromZoneType(
                                                        StoreZoneType.Zone4);
                                                zoneType = StoreZoneType.Zone4;
                                              });
                                            },
                                            zone: stringFromEnum
                                                .getStringFromZoneType(
                                                    StoreZoneType.Zone4),
                                            zoneSelected: zoneType,
                                            zoneType: StoreZoneType.Zone4,
                                          ),
                                          SizedBox(
                                              width: storeZoneSizedBoxWidth),
                                          StoreZone(
                                            onPressed: () {
                                              setState(() {
                                                storeZone = stringFromEnum
                                                    .getStringFromZoneType(
                                                        StoreZoneType.Zone5);
                                                zoneType = StoreZoneType.Zone5;
                                              });
                                            },
                                            zone: stringFromEnum
                                                .getStringFromZoneType(
                                                    StoreZoneType.Zone5),
                                            zoneSelected: zoneType,
                                            zoneType: StoreZoneType.Zone5,
                                          ),
                                          SizedBox(
                                              width: storeZoneSizedBoxWidth),
                                          StoreZone(
                                            onPressed: () {
                                              setState(() {
                                                storeZone = stringFromEnum
                                                    .getStringFromZoneType(
                                                        StoreZoneType.Zone6);
                                                zoneType = StoreZoneType.Zone6;
                                              });
                                            },
                                            zone: stringFromEnum
                                                .getStringFromZoneType(
                                                    StoreZoneType.Zone6),
                                            zoneSelected: zoneType,
                                            zoneType: StoreZoneType.Zone6,
                                          )
                                        ],
                                      ),
                                    ];
                                  }

                                  return Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: children,
                                  );
                                }),
                            Padding(
                                padding: sendButtonPadding,
                                child: Row(
                                  children: [
                                    GeneralPickupButton(
                                      onPressed: () async {
                                        orderPickupSuccess
                                            ? newOrderPickup()
                                            : sendOrderPickup();
                                      },
                                      buttonText:
                                          orderPickupSuccess ? 'Nou' : 'Enviar',
                                    ),
                                    SizedBox(width: storeZoneSizedBoxWidth),
                                    FutureBuilder<ApiResponse>(
                                        future: _futureOrderPickup,
                                        builder:
                                            (context, snapshotOrderPickup) {
                                          List<Widget> children;
                                          if (snapshotOrderPickup
                                                  .connectionState ==
                                              ConnectionState.waiting) {
                                            children = <Widget>[
                                              CircularProgressIndicator(),
                                            ];
                                          } else if (snapshotOrderPickup
                                                  .connectionState ==
                                              ConnectionState.none) {
                                            children = <Widget>[
                                              SizedBox(
                                                  width: storeZoneSizedBoxWidth)
                                            ];
                                          } else {
                                            if (snapshotOrderPickup.hasData) {
                                              if (snapshotOrderPickup
                                                  .data.success) {
                                                children = <Widget>[
                                                  Icon(
                                                    Icons.check_circle_outline,
                                                    color: Color(0xFFb7ce00),
                                                    size: iconSize,
                                                  ),
                                                  Text("Afegit correctament"),
                                                ];
                                              } else {
                                                children = <Widget>[
                                                  Icon(
                                                    Icons.error_outline,
                                                    color: Colors.red,
                                                    size: iconSize,
                                                  ),
                                                  Text(snapshotOrderPickup
                                                      .data.message)
                                                ];
                                              }
                                            } else if (snapshotOrderPickup
                                                .hasError) {
                                              children = <Widget>[
                                                Icon(
                                                  Icons.error_outline,
                                                  color: Colors.red,
                                                  size: iconSize,
                                                ),
                                                Text("error")
                                              ];
                                            } else {
                                              children = <Widget>[
                                                SizedBox(
                                                    width:
                                                        storeZoneSizedBoxWidth)
                                              ];
                                            }
                                          }
                                          return Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: children,
                                          );
                                        }),
                                  ],
                                )),
                          ]))
                    ]),
              )
            ],
          ))),
      floatingActionButton: ScanButton(onPressed: () {
        _scanCode();
      }),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}

class GeneralPickupButton extends StatelessWidget {
  GeneralPickupButton({@required this.onPressed, @required this.buttonText});

  final Function onPressed;
  final String buttonText;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Text("$buttonText",
          textAlign: TextAlign.left,
          maxLines: 1,
          style: validationButtonTextStyle),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: validationButtonWidth,
        height: validationButtonHeight,
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(validationbBorderRadius),
          side: BorderSide(
              color: createMaterialColor(Color(0xFF00644b)), width: 1)),
      fillColor: createMaterialColor(Color(0xFF00644b)),
    );
  }
}

class TextInputButton extends StatelessWidget {
  TextInputButton(
      {@required this.child,
      @required this.onPressed,
      @required this.inputSelected,
      @required this.width,
      @required this.validated});

  final Function onPressed;
  final Widget child;
  final bool inputSelected;
  final double width;
  final bool validated;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onPressed,
        child: SizedBox(
            height: textInputHeight,
            width: width,
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey.shade200,
                    border: (!validated)
                        ? Border.all(
                            color: createMaterialColor(
                                Color.fromRGBO(255, 87, 51, 1)),
                            width: 3)
                        : inputSelected
                            ? Border.all(
                                color: createMaterialColor(Color(0xFFb7ce00)),
                                width: 3)
                            : Border.all(color: Colors.grey.shade500),
                    borderRadius: new BorderRadius.circular(inputBorderRadius)),
                child: child)));
  }
}

class TextInput extends StatelessWidget {
  TextInput(
      {@required this.inputTitle,
      @required this.inputValue,
      @required this.onPressed,
      @required this.inputType,
      @required this.inputSelected,
      @required this.validated});

  final String inputTitle;
  final String inputValue;
  final Function onPressed;
  final PickupInputType inputType;
  final PickupInputType inputSelected;
  final bool validated;

  @override
  Widget build(BuildContext context) {
    return TextInputButton(
        onPressed: this.onPressed,
        inputSelected: (inputSelected == inputType) ? true : false,
        width:
            (inputType == PickupInputType.NumOf) ? numOfInputWidth : inputWidth,
        validated: validated,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: textInputPadding,
                    child: Text("$inputTitle",
                        textAlign: TextAlign.left,
                        maxLines: 1,
                        style: inputTitleTextStyle),
                  ),
                  Padding(
                    padding: textInputPadding,
                    child: Text("$inputValue",
                        textAlign: TextAlign.left, style: inputValueTextStyle),
                  ),
                ]),
          ],
        ));
  }
}

class StoreZone extends StatelessWidget {
  StoreZone(
      {@required this.zone,
      @required this.onPressed,
      @required this.zoneType,
      @required this.zoneSelected});

  final String zone;
  final Function onPressed;
  final StoreZoneType zoneType;
  final StoreZoneType zoneSelected;

  @override
  Widget build(BuildContext context) {
    return StoreZoneButton(
      onPressed: this.onPressed,
      zoneSelected: (zoneSelected == zoneType) ? true : false,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("$zone", style: zoneButtonTextStyle),
          ]),
    );
  }
}

class StoreZoneButton extends StatelessWidget {
  StoreZoneButton(
      {@required this.child,
      @required this.onPressed,
      @required this.zoneSelected});

  final Widget child;
  final Function onPressed;
  final bool zoneSelected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onPressed,
        child: SizedBox(
            height: storeZoneButtonHeight,
            width: storeZoneButtonWidth,
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey.shade200,
                    border: zoneSelected
                        ? Border.all(
                            color: createMaterialColor(Color(0xFFb7ce00)),
                            width: 3)
                        : Border.all(color: Colors.grey.shade500),
                    borderRadius:
                        new BorderRadius.circular(storeZoneBorderRadius)),
                child: child)));
  }
}

class ScanButton extends StatelessWidget {
  ScanButton({
    @required this.onPressed,
  });
  final Function onPressed;
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        elevation: 0,
        child: Icon(Icons.qr_code_scanner_rounded),
        onPressed: onPressed);
  }
}
