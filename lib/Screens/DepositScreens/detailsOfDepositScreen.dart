// import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ondunova_app/Api/apiProvider.dart';
import 'package:ondunova_app/Class/apiResponseClass.dart';
// import 'package:intl/intl.dart';
// import 'package:provider/provider.dart';

// import 'package:swipedetector/swipedetector.dart';

import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';
import 'package:ondunova_app/Components/alertDialog.dart';
import 'package:ondunova_app/Components/generalButton.dart';
import 'package:ondunova_app/Constants/Enums/enums.dart';
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';
import 'package:ondunova_app/Styles/Screens/DepositScreens/detailsOfDepositScreenStyles.dart';

// import 'package:ondunova_app/Api/apiProvider.dart';
// import 'package:ondunova_app/Constants/enums.dart';

import 'package:ondunova_app/utils/materialColorFromHex.dart';
import 'package:ondunova_app/utils/ordersActions.dart';

class DetailsOfDepositScreen extends StatefulWidget {
  final void Function(int, Order) returnPalet;
  final void Function(int) changeTab;
  final OrderJob orderJob;

  DetailsOfDepositScreen(
      {Key key, this.returnPalet, this.changeTab, @required this.orderJob})
      : super(key: key);
  @override
  _DetailsOfDepositScreenState createState() => _DetailsOfDepositScreenState();
}

class _DetailsOfDepositScreenState extends State<DetailsOfDepositScreen> {
  ApiProvider _apiProvider = ApiProvider();
  OrderJob orderJob;
  bool loading = false;

  ApiResponse orderDepositByOfResponse;
  ApiResponse orderDepositResponse;

  OrderActions _orderActions = OrderActions();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      orderJob = widget.orderJob;
    });
  }

  getCardColor(orderJobDisplayStatus) {
    var cardColor;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      cardColor = Colors.blue[600];
    } else if (describeEnum(DepositState.C0) == orderJobDisplayStatus.trim()) {
      cardColor = Colors.blue[200];
    } else {
      cardColor = Colors.amber[200];
    }

    return cardColor;
  }

  getOrderStatus(orderStatus) {
    var orderStatusName;
    if (orderStatus == describeEnum(OrderStatus.EnRecogida)) {
      orderStatusName = OrderStatus.EnRecogida.name;
    } else if (orderStatus == describeEnum(OrderStatus.EnTransito)) {
      orderStatusName = OrderStatus.EnTransito.name;
    } else if (orderStatus == describeEnum(OrderStatus.EnAlmacen)) {
      orderStatusName = OrderStatus.EnAlmacen.name;
    } else {
      orderStatusName = OrderStatus.EnProduccion.name;
    }

    return orderStatusName;
  }

  getNumOfStyle(orderJobDisplayStatus) {
    var cardColor = numOfItemTextStyle;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      cardColor = numOfItemC1TextStyle;
    }
    return cardColor;
  }

  getTitleStyle(orderJobDisplayStatus) {
    var cardColor = titleTextStyle;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      cardColor = titleC1TextStyle;
    }
    return cardColor;
  }

  getSubtitleStyle(orderJobDisplayStatus) {
    var style = subTitleTextStyle;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      style = subTitleC1TextStyle;
    }
    return style;
  }

  getDetailStyle(orderJobDisplayStatus) {
    var style = detailOrderTextStyle;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      style = detailC1OrderTextStyle;
    }
    return style;
  }

  void _showDialog(Order order) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomAlertDialog(
            title: "Portara a màquina el palet nº " +
                order.numPalet.toString() +
                " amb número de OF " +
                order.numOf,
            subtitle: "està segur de voler continuar?",
            onValidationPressed: () async {
              Navigator.of(context).pop();
              setState(() {
                loading = true;
              });

              orderDepositResponse = await _apiProvider.newOrderDeposit(order);
              if (orderDepositResponse.success) {
                orderJob.orders
                    .removeWhere((element) => (element.id == order.id));
              }
              /* List<Order> orderList =
                  _orderActions.getOrdersFromResponse(orderDepositResponse);

              List<Order> orderListFiltered =
                  _orderActions.filterByOrderStatusAndOf(order, orderList); */

              setState(() {
                loading = false;
                /* orderJob.orders = orderListFiltered; */
              });
            },
            onRejectPressed: () {
              Navigator.of(context).pop();
            },
            cancelText: 'Cancelar',
            validateText: 'Validar',
          );
        });
  }

  void _showPortarTotsDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomAlertDialog(
            title: "Portara a màquina tots el palets amb número de OF " +
                orderJob.orderNumber,
            subtitle: "està segur de voler continuar?",
            onValidationPressed: () async {
              Navigator.of(context).pop();
              setState(() {
                loading = true;
              });

              orderDepositByOfResponse =
                  await _apiProvider.newOrderDepositByOf(orderJob.orderNumber);

              if (orderDepositByOfResponse.success) {
                setState(() {
                  loading = false;
                  orderJob.orders = [];
                });
              } else {
                setState(() {
                  loading = false;
                });
              }
            },
            onRejectPressed: () {
              Navigator.of(context).pop();
            },
            validateText: 'Validar',
            cancelText: 'Cancelar',
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: createMaterialColor(Color(0xFF00644b)),
            centerTitle: true,
            title: Text('Detall Ordre de Fabricació', style: appBarTextStyle),
            iconTheme: IconThemeData(
              color: Color(0xFFb7ce00),
            ),
          ),
          body: Center(
            child: CircularProgressIndicator(),
          ));
    } else if (orderJob.orders.length > 0) {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: createMaterialColor(Color(0xFF00644b)),
            centerTitle: true,
            title: Text('Detall Ordre de Fabricació', style: appBarTextStyle),
            iconTheme: IconThemeData(
              color: Color(0xFFb7ce00), //change your color here
            ),
          ),
          body: CustomScrollView(slivers: [
            SliverList(
                delegate: SliverChildListDelegate(
              List.generate(orderJob.orders.length, (index) {
                var pickupDate = "";
                var cardColor = getCardColor(orderJob.displayStatus);
                var orderStatus =
                    getOrderStatus(orderJob.orders[index].orderStatus);

                var numOfStyle = getNumOfStyle(orderJob.displayStatus);
                var titleStyle = getTitleStyle(orderJob.displayStatus);
                var subTitleStyle = getSubtitleStyle(orderJob.displayStatus);
                var detailOrderStyle = getDetailStyle(orderJob.displayStatus);

                if (orderJob.orders[index].pickupDate != null) {
                  pickupDate = DateFormat('yyyy/MM/dd HH:mm:ss').format(
                      DateTime.parse(orderJob.orders[index].pickupDate));
                }

                return Container(
                    margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                    decoration: BoxDecoration(color: cardColor),
                    child: Column(children: [
                      ListTile(
                          tileColor: cardColor,
                          leading: Text(orderJob.orders[index].numOf,
                              style: numOfStyle),
                          title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(orderJob.orders[index].machineName,
                                    style: titleStyle),
                                Text(
                                    orderJob.orders[index].storePosition +
                                        " - Palet " +
                                        orderJob.orders[index].numPalet
                                            .toString(),
                                    style: titleStyle),
                              ]),
                          subtitle: Text(pickupDate, style: subTitleStyle),
                          trailing:
                              Row(mainAxisSize: MainAxisSize.min, children: [
                            DepositButton(
                              onPressed: () {
                                _showDialog(orderJob.orders[index]);
                              },
                            )
                          ])),
                      Divider(),
                      Padding(
                        padding: EdgeInsets.all(15),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                      'Quantitat: ${orderJob.orders[index].quantity}',
                                      style: detailOrderStyle,
                                    ),
                                    Text(
                                      'Client: ${orderJob.orders[index].clientName.trim()}',
                                      style: detailOrderStyle,
                                    ),
                                    Text(
                                        'Total palets: ${orderJob.noPallets.toString()}',
                                        style: detailOrderStyle)
                                  ]),
                            ]),
                      )
                    ]));
              }),
            )),
            SliverFillRemaining(
                hasScrollBody: false,
                child: Center(
                  child: Padding(
                    padding:
                        EdgeInsets.only(bottom: 15, left: 0, right: 0, top: 10),
                    child: GeneralButton(
                      onPressed: () {
                        _showPortarTotsDialog();
                      },
                      color: Color(0xFF00644b),
                      buttonText: "Portar tots",
                    ),
                  ),
                ))
          ]));
    } else {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: createMaterialColor(Color(0xFF00644b)),
            centerTitle: true,
            title: Text('Detall Odre de Fabricació', style: appBarTextStyle),
            iconTheme: IconThemeData(
              color: Color(0xFFb7ce00), //change your color here
            ),
          ),
          body: Center(
            child: Text(
              "No hi ha palets al magatzem per la OF nº ${orderJob.orderNumber}",
              style: detailOrderTextStyle,
            ),
          ));
    }
  }
}

class DepositButton extends StatelessWidget {
  DepositButton({this.onPressed});

  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Text("Portar a Máquina", style: infoButtonTextStyle),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: 200,
        height: 60,
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      fillColor: createMaterialColor(Color(0xFF00644b)),
    );
  }
}
