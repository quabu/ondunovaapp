import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ondunova_app/Class/apiResponseClass.dart';

import 'package:ondunova_app/Screens/DepositScreens/detailsOfDepositScreen.dart';
import 'package:ondunova_app/utils/ordersActions.dart';
import 'package:ondunova_app/utils/ordersJobsActions.dart';
import 'package:swipedetector/swipedetector.dart';
import "package:collection/collection.dart";
//Constants
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';
import 'package:ondunova_app/Constants/Enums/machineCodesEnum.dart';
//Styles
import 'package:ondunova_app/Styles/Screens/DepositScreens/depositScreenStyles.dart';
//Utils
import 'package:ondunova_app/utils/materialColorFromHex.dart';
//Class
import '../../Class/orderClass.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';
import 'package:ondunova_app/Class/groupedOrderJobsClass.dart';
//Api
import '../../Api/apiProvider.dart';
//Components
import 'package:ondunova_app/Components/machineList.dart';

class DepositScreen extends StatefulWidget {
  final void Function(int) changeTab;
  DepositScreen({Key key, @required this.changeTab}) : super(key: key);

  @override
  _DepositScreenState createState() => _DepositScreenState();
}

class _DepositScreenState extends State<DepositScreen> {
  Future<List<OrderJob>> _futureOrderJobs;
  Future<bool> _futureOrderDeposit;

  ApiProvider _apiProvider = ApiProvider();
  OrderActions _orderActions = OrderActions();
  OrderJobsActions _orderJobsActions = OrderJobsActions();

  bool isLoading = true;

  ApiResponse orderJobsResponse;
  ApiResponse orderResponse;

  List<bool> indexDropdown;
  List<OrderJob> orderJobs;
  List<GroupedOrderJobs> groupedOrderJobs;

  //List of orderJobs by machine
  GroupedOrderJobs curioniOrderJobs;
  GroupedOrderJobs martinOrderJobs;
  GroupedOrderJobs gopfertOrderJobs;
  GroupedOrderJobs serraOrderJobs;
  GroupedOrderJobs rtaOrderJobs;
  GroupedOrderJobs rapidexOrderJobs;

  void initState() {
    super.initState();
    getOrderJobs();
  }

  getOrderJobs() async {
    setState(() {
      isLoading = true;
    });
    orderJobsResponse = await _apiProvider.getOrderJobs();
    orderResponse = await _apiProvider.getOrders();

    if (orderJobsResponse.success && orderResponse.success) {
      List<OrderJob> currentOrderJobs =
          _orderJobsActions.getOrdersJobsFromResponse(orderJobsResponse);
      List<Order> orders = _orderActions.getOrdersFromResponse(orderResponse);
      List<OrderJob> ordersJobsMerged =
          _orderJobsActions.mergeOrdersToOrderJobs(
              currentOrderJobs, orders, OrderStatus.EnAlmacen);
      await setOrderJobs(ordersJobsMerged);

      setState(() {
        orderJobs = currentOrderJobs;
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  setOrderJobs(orderJobsResponse) async {
    var mapedOrderJobs;
    mapedOrderJobs =
        groupBy(orderJobsResponse, (OrderJob order) => order.machineCode);

    mapedOrderJobs.forEach((group, orderJobs) {
      orderJobs.removeRange(3, orderJobs.length);
      if (group.trim() == MachineTypes.Curioni.code) {
        curioniOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.Martin.code) {
        martinOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.Gopfert.code) {
        gopfertOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.Serra.code) {
        serraOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.RTA.code) {
        rtaOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.Rapidex.code) {
        rapidexOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      }
    });
  }

  goToDetailOrderJob(OrderJob orderJob, BuildContext context) {
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    DetailsOfDepositScreen(orderJob: orderJob)))
        .then((value) => getOrderJobs());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: createMaterialColor(Color(0xFF00644b)),
            centerTitle: true,
            title: Text('Portar a Màquina', style: appBarTextStyle),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      getOrderJobs();
                    },
                    child: Icon(Icons.refresh,
                        size: 30.0, color: Color(0xFFb7ce00)),
                  )),
            ]),
        body: SwipeDetector(
            onSwipeLeft: () {
              widget.changeTab(3);
            },
            onSwipeRight: () {
              widget.changeTab(1);
            },
            child: Center(
                child: (isLoading)
                    ? Center(
                        child: SizedBox(
                            height: 30,
                            width: 30,
                            child: CircularProgressIndicator()))
                    : (!orderJobsResponse.success)
                        ? Center(
                            child: Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: iconSize,
                          ))
                        : Container(
                            child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                  child: MachineList(
                                listOrderJob: curioniOrderJobs.orderJobs,
                                openDetail: (orderJob) {
                                  goToDetailOrderJob(orderJob, context);
                                },
                                title: 'Curioni',
                              )),
                              Expanded(
                                  child: MachineList(
                                listOrderJob: rapidexOrderJobs.orderJobs,
                                openDetail: (orderJob) {
                                  goToDetailOrderJob(orderJob, context);
                                },
                                title: 'Rapidex',
                              )),
                              Expanded(
                                  child: MachineList(
                                listOrderJob: rtaOrderJobs.orderJobs,
                                openDetail: (orderJob) {
                                  goToDetailOrderJob(orderJob, context);
                                },
                                title: 'RTA',
                              )),
                              Expanded(
                                  child: MachineList(
                                listOrderJob: serraOrderJobs.orderJobs,
                                openDetail: (orderJob) {
                                  goToDetailOrderJob(orderJob, context);
                                },
                                title: 'Serra',
                              )),
                              Expanded(
                                  child: MachineList(
                                listOrderJob: martinOrderJobs.orderJobs,
                                openDetail: (orderJob) {
                                  goToDetailOrderJob(orderJob, context);
                                },
                                title: 'Martin',
                              )),
                              Expanded(
                                  child: MachineList(
                                listOrderJob: gopfertOrderJobs.orderJobs,
                                openDetail: (orderJob) {
                                  goToDetailOrderJob(orderJob, context);
                                },
                                title: 'Gopfert',
                              )),
                            ],
                          )))));
  }
}
