import 'package:flutter/material.dart';
import 'package:ondunova_app/Class/apiResponseClass.dart';
import 'package:ondunova_app/Class/groupedOrderClass.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';
import 'package:ondunova_app/Components/alertDialog.dart';
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';

import 'package:ondunova_app/Constants/storeFilterTypes.dart';
import 'package:ondunova_app/Screens/StoreScreens/storeZoneScreen.dart';

import 'package:swipedetector/swipedetector.dart';

//Api
import 'package:ondunova_app/Api/apiProvider.dart';

//Class
import 'package:ondunova_app/Class/orderClass.dart';

//Screens

//Styles
import 'package:ondunova_app/Styles/Screens/StoreScreens/storeScreenStyles.dart';

//Utils
import 'package:ondunova_app/utils/materialColorFromHex.dart';
import 'package:ondunova_app/utils/ordersActions.dart';

class StoreScreen extends StatefulWidget {
  /* final TabController tabController; */
  final void Function(int, Order) returnPalet;
  final void Function(int) changeTab;
  StoreScreen({Key key, @required this.returnPalet, @required this.changeTab})
      : super(key: key);

  @override
  _StoreScreenState createState() => new _StoreScreenState();
}

class _StoreScreenState extends State<StoreScreen> {
  Future<List<Order>> _futureOrders;
  Future<List<OrderJob>> _futureOrderJobs;
  /* Future<bool> _futureDeleteOrder; */
  ApiProvider _apiProvider = ApiProvider();
  ApiResponse ordersResponse;

  bool isLoading = true;

  List<OrderJob> orderJob;
  List<Order> listOrders;
  String currentChoice = StoreFilterTypes.ByNumOF;

  OrderActions _orderActions = OrderActions();
  var groupedList;

  void initState() {
    super.initState();
    loadOrderJobs(currentChoice);
  }

  loadOrderJobs(String choice) async {
    setState(() {
      isLoading = true;
    });

    ordersResponse = await _apiProvider.getOrders();
    if (ordersResponse.success) {
      List<Order> orders = _orderActions.getOrdersFromResponse(ordersResponse);
      var ordersData =
          _orderActions.getStoreOrdersAndGroupedOrders(orders, choice);
      setState(() {
        listOrders = ordersData[0];
        groupedList = ordersData[1];
        currentChoice = choice;
        isLoading = false;
      });
    }
  }

  void _showDialog(Order order) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomAlertDialog(
            title: "S'eliminarà el palet nº " +
                order.numPalet.toString() +
                " amb número de OF " +
                order.numOf,
            subtitle: "està segur de voler continuar?",
            onValidationPressed: () {
              setState(() {
                isLoading = true;
              });
              _apiProvider.deleteOrder(order.id).then((deleteOrderResponse) =>
                  setDeleteSuccess(deleteOrderResponse));
              Navigator.of(context).pop();
            },
            onRejectPressed: () {
              Navigator.of(context).pop();
            },
            cancelText: 'Cancelar',
            validateText: 'Validar',
          );
        });
  }

  setDeleteSuccess(deleteOrderResponse) {
    if (deleteOrderResponse.success) {
      loadOrderJobs(currentChoice);
    }
  }

  returnOrder(Order order) {
    widget.returnPalet(1, order);
  }

  changeZone(Order order) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => StoreZoneScreen(
                  order: order,
                ))).then((value) => loadOrderJobs(currentChoice));
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
            backgroundColor: createMaterialColor(Color(0xFF00644b)),
            centerTitle: true,
            title: Text('Gestió Magatzem',
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Color(0xFFe6e6e6))),
            actions: <Widget>[
              PopupMenuButton<String>(
                  icon: Icon(Icons.filter_list,
                      size: 30.0, color: Color(0xFFb7ce00)),
                  onSelected: loadOrderJobs,
                  elevation: 2,
                  itemBuilder: (BuildContext context) {
                    return StoreFilterTypes.choices.map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Text(choice,
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                color: choice == currentChoice
                                    ? Color(0xFFb7ce00)
                                    : Colors.black)),
                      );
                    }).toList();
                  }),
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () => loadOrderJobs(currentChoice),
                    child: Icon(Icons.refresh,
                        size: 30.0, color: Color(0xFFb7ce00)),
                  )),
            ]),
        body: SwipeDetector(
            onSwipeRight: () {
              widget.changeTab(3);
            },
            child: (isLoading)
                ? Center(
                    child: SizedBox(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator()))
                : (!ordersResponse.success)
                    ? Center(
                        child: Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: iconSize,
                      ))
                    : Container(
                        height: screenSize.height,
                        width: screenSize.width,
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: groupedList.length,
                          itemBuilder: (context, index) => Card(
                              elevation: 2,
                              color: Color(0xFF00644b),
                              margin: EdgeInsets.all(10),
                              child: Column(
                                children: [
                                  ExpansionTile(
                                    title: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                            groupedList[index].group.toString(),
                                            style: numOfItemTextStyle),
                                        (currentChoice ==
                                                StoreFilterTypes.ByNumOF)
                                            ? Text(
                                                groupedList[index]
                                                    .orders[0]
                                                    .clientName
                                                    .toString(),
                                                style: subTitleTextStyle)
                                            : SizedBox(width: 20)
                                      ],
                                    ),
                                    subtitle: Text(
                                      "Quantitat de palets: " +
                                          groupedList[index]
                                              .orders
                                              .length
                                              .toString(),
                                      style: subTitleTextStyle,
                                    ),
                                    trailing: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [Icon(Icons.arrow_drop_down)],
                                    ),
                                    children: [
                                      Divider(),
                                      Visibility(
                                          visible: true,
                                          maintainSize: false,
                                          maintainAnimation: true,
                                          maintainState: true,
                                          child: ListView.builder(
                                              shrinkWrap: true,
                                              physics: ClampingScrollPhysics(),
                                              itemCount: groupedList[index]
                                                  .orders
                                                  .length,
                                              itemBuilder: (context,
                                                      indexOrders) =>
                                                  Card(
                                                      elevation: 2,
                                                      color:
                                                          createMaterialColor(
                                                              Color(
                                                                  0xFFe6e6e6)),
                                                      margin:
                                                          EdgeInsets.all(10),
                                                      child: Column(children: [
                                                        ListTile(
                                                            leading: currentChoice ==
                                                                    StoreFilterTypes
                                                                        .ByNumOF
                                                                ? Text(
                                                                    groupedList[
                                                                            index]
                                                                        .orders[
                                                                            indexOrders]
                                                                        .storePosition,
                                                                    style:
                                                                        titleOrderTextStyle)
                                                                : Text(
                                                                    groupedList[
                                                                            index]
                                                                        .orders[
                                                                            indexOrders]
                                                                        .numOf,
                                                                    style:
                                                                        titleOrderTextStyle),
                                                            title: Text(
                                                                "Número de palet: " +
                                                                    groupedList[
                                                                            index]
                                                                        .orders[
                                                                            indexOrders]
                                                                        .numPalet
                                                                        .toString(),
                                                                style:
                                                                    infoOrderTextStyle),
                                                            subtitle: Text(
                                                                "Quantitat: " +
                                                                    groupedList[
                                                                            index]
                                                                        .orders[
                                                                            indexOrders]
                                                                        .quantity
                                                                        .toString(),
                                                                style:
                                                                    infoOrderTextStyle),
                                                            trailing: Row(
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .min,
                                                              children: [
                                                                Padding(
                                                                  padding:
                                                                      orderButtonsPadding,
                                                                  child:
                                                                      SwitchZoneButton(
                                                                    onPressed:
                                                                        () {
                                                                      changeZone(
                                                                        groupedList[index]
                                                                            .orders[indexOrders],
                                                                      );
                                                                    },
                                                                  ),
                                                                ),
                                                                Padding(
                                                                    padding:
                                                                        orderButtonsPadding,
                                                                    child: EditButton(
                                                                        onPressed:
                                                                            () {
                                                                      returnOrder(
                                                                          groupedList[index]
                                                                              .orders[indexOrders]);
                                                                    })),
                                                                DeleteButton(
                                                                    onPressed:
                                                                        () {
                                                                  _showDialog(groupedList[
                                                                              index]
                                                                          .orders[
                                                                      indexOrders]);
                                                                }),
                                                              ],
                                                            )),
                                                      ]))))
                                    ],
                                  ),
                                ],
                              )),
                        ))));
  }
}

class DeleteButton extends StatelessWidget {
  DeleteButton({this.onPressed});

  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Icon(
        Icons.delete,
        size: 40,
        color: createMaterialColor(Color(0xFFb7ce00)),
      ),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: 60,
        height: 60,
      ),
      shape: CircleBorder(),
      fillColor: createMaterialColor(Color(0xFF00644b)),
    );
  }
}

class EditButton extends StatelessWidget {
  EditButton({this.onPressed});

  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Icon(
        Icons.edit,
        size: 40,
        color: createMaterialColor(Color(0xFFb7ce00)),
      ),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: 60,
        height: 60,
      ),
      shape: CircleBorder(),
      fillColor: createMaterialColor(Color(0xFF00644b)),
    );
  }
}

class SwitchZoneButton extends StatelessWidget {
  SwitchZoneButton({this.onPressed});

  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Icon(
        Icons.swap_horizontal_circle,
        size: 40,
        color: createMaterialColor(Color(0xFFb7ce00)),
      ),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: 60,
        height: 60,
      ),
      shape: CircleBorder(),
      fillColor: createMaterialColor(Color(0xFF00644b)),
    );
  }
}

/* class StoreAppBar extends StatelessWidget implements PreferredSizeWidget {
  StoreAppBar({this.changeFilter, this.refresh, this.currentChoice});

  final Function changeFilter;
  final Function refresh;
  final String currentChoice;

  @override
  Size get preferredSize => Size.fromHeight(50);

  @override
  Widget build(BuildContext context) {
    return AppBar(
        backgroundColor: createMaterialColor(Color(0xFF00644b)),
        centerTitle: true,
        title: Text('Gestió Magatzem',
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Color(0xFFe6e6e6))),
        actions: <Widget>[
          PopupMenuButton<String>(
              icon:
                  Icon(Icons.filter_list, size: 30.0, color: Color(0xFFb7ce00)),
              onSelected: changeFilter,
              elevation: 2,
              itemBuilder: (BuildContext context) {
                return StoreFilterTypes.choices.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice,
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: choice == currentChoice
                                ? Color(0xFFb7ce00)
                                : Colors.black)),
                  );
                }).toList();
              }),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                /* onTap: () => refresh(), */
                child:
                    Icon(Icons.refresh, size: 30.0, color: Color(0xFFb7ce00)),
              )),
        ]);
  }
} */