import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
//Api
import 'package:ondunova_app/Api/apiProvider.dart';
import 'package:ondunova_app/Class/apiResponseClass.dart';
//Class
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Components/storeZoneChange.dart';
//Enums
import 'package:ondunova_app/Constants/Enums/storeZoneEnum.dart';
//Screens
import 'package:ondunova_app/Styles/Screens/StoreScreens/zoneScreenStyles.dart';
//Utils
import 'package:ondunova_app/utils/materialColorFromHex.dart';
import 'package:ondunova_app/utils/stringFromEnum.dart';
//Components
import 'package:ondunova_app/Components/generalButton.dart';

class StoreZoneScreen extends StatefulWidget {
  final Order order;
  final void Function(int) changeTab;
  StoreZoneScreen({Key key, this.order, this.changeTab}) : super(key: key);

  @override
  _StoreZoneScreenState createState() => new _StoreZoneScreenState();
}

class _StoreZoneScreenState extends State<StoreZoneScreen> {
  ApiProvider _apiProvider = ApiProvider();
  ApiResponse zoneSelectResponse;

  Order order;
  StringFromEnum stringFromEnum = StringFromEnum();

  bool isLoading = false;
  bool isError = false;

  StoreZoneType zoneTypeSelected = StoreZoneType.Zone1;

  bool zoneExist = false;
  bool zoneSelectSuccess = false;

  //Input values
  String storeZone = "";

  @override
  void initState() {
    super.initState();

    StoreZoneType currentStoreZone =
        stringFromEnum.getStoreZoneValue(widget.order.storePosition);

    setState(() {
      order = widget.order;
      zoneTypeSelected = currentStoreZone;
      storeZone = stringFromEnum.getStringFromZoneType(zoneTypeSelected);
    });
  }

  goBack() {
    Navigator.pop(context);
  }

  retry() {
    setState(() {
      isError = false;
      zoneSelectResponse = null;
    });
  }

  newZoneSelect() async {
    setState(() {
      isLoading = true;
    });

    var zoneSelect = new Order(
        id: order.id,
        numOf: order.numOf,
        numPalet: order.numPalet,
        quantity: order.quantity,
        storePosition: storeZone,
        orderStatus: order.orderStatus,
        user: order.user,
        pickupDate: order.pickupDate,
        depositDate: order.depositDate,
        machineId: order.machineId,
        machineName: order.machineName,
        clientId: order.clientId,
        clientName: order.clientName);

    zoneSelectResponse = await _apiProvider.updateOrderPickup(zoneSelect);

    if (zoneSelectResponse.success) {
      goBack();
      setState(() {
        isLoading = false;
        zoneSelect = zoneSelect;
        isError = !zoneSelectResponse.success;
      });
    } else {
      setState(() {
        isLoading = false;
        zoneSelect = zoneSelect;
        isError = zoneSelectResponse.success;
      });
    }
  }

  setNewStoreZoneType(StoreZoneType storeZoneType, String storeZoneTypeString) {
    setState(() {
      zoneTypeSelected = storeZoneType;
      storeZone = storeZoneTypeString;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Canviar de zona', style: appBarTextStyle),
          backgroundColor: createMaterialColor(Color(0xFF00644b)),
          centerTitle: true,
          iconTheme: IconThemeData(
            color: Color(0xFFb7ce00),
          ),
        ),
        body: (isLoading)
            ? Center(
                child: SizedBox(
                    height: 30, width: 30, child: CircularProgressIndicator()))
            : (isError)
                ? Center(
                    child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: iconSize,
                      ),
                      SizedBox(height: 20),
                      Text(zoneSelectResponse.message),
                      SizedBox(height: 20),
                      GeneralButton(
                          onPressed: () async {
                            retry();
                          },
                          color: Color(0xFF00644b),
                          buttonText: 'Tornar a provar'),
                    ],
                  ))
                : /* (zoneSelectResponse != null)
                    ? Center(
                        child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.check_circle_outline,
                            color: Color(0xFFb7ce00),
                            size: iconSize,
                          ),
                          SizedBox(height: 20),
                          Text(zoneSelectResponse.message),
                          SizedBox(height: 20),
                          GeneralButton(
                              onPressed: () async {
                                goBack();
                              },
                              color: Color(0xFF00644b),
                              buttonText: 'Acceptar'),
                        ],
                      ))
                    : */
                StoreZoneChange(
                    order: order,
                    zoneSelected: zoneTypeSelected,
                    setStoreZone: (storeZoneType, storeZoneTypeString) {
                      setNewStoreZoneType(storeZoneType, storeZoneTypeString);
                    },
                    actionZone: () => newZoneSelect()));
  }
}
