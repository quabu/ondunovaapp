import 'package:flutter/material.dart';
import 'package:swipedetector/swipedetector.dart';

//Screens
import 'package:ondunova_app/Screens/DepositScreens/depositScreen.dart';

//Utils
import 'package:ondunova_app/utils/icon_string_util.dart';
import 'package:ondunova_app/providers/menu_provider.dart';
import 'package:ondunova_app/utils/materialColorFromHex.dart';

class HomeScreen extends StatefulWidget {
  /* final TabController tabController; */
  final void Function(int) changeTab;
  HomeScreen({Key key, @required this.changeTab}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: createMaterialColor(Color(0xFF00644b)),
        centerTitle: true,
        title: Text('Home',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Color(0xFFe6e6e6),
                fontSize: 28)),
      ),
      body: SwipeDetector(
          onSwipeLeft: () {
            widget.changeTab(1);
          },
          child: Container(
            padding: EdgeInsets.all(40),
            child: _lista(widget.changeTab),
            color: Colors.white,
          )),
    );
  }

  Widget _lista(Function(int) changeTab) {
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _listaItems(snapshot.data, context, changeTab),
        );
      },
    );
  }
}

List<Widget> _listaItems(
    List<dynamic> data, BuildContext context, Function(int) changeTab) {
  final List<Widget> opciones = [];

  data.forEach((opt) {
    final widgetTemp = Padding(
        padding: EdgeInsets.only(bottom: 15, left: 0, right: 0, top: 15),
        child: ListTile(
          title: Text(opt['texto'],
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24)),
          leading: getIcon(opt['icon']),
          // trailing: Icon(Icons.keyboard_arrow_right_outlined, color: Colors.black),
          onTap: () {
            changeTab(opt['navigator']);
          },
        ));

    opciones..add(widgetTemp);
  });

  return opciones;
}
