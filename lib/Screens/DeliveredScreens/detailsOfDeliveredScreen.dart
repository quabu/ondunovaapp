// import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ondunova_app/Api/apiProvider.dart';
import 'package:ondunova_app/Class/apiResponseClass.dart';
// import 'package:intl/intl.dart';
// import 'package:provider/provider.dart';

// import 'package:swipedetector/swipedetector.dart';

import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';
import 'package:ondunova_app/Constants/Enums/enums.dart';
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';
import 'package:ondunova_app/Screens/DeliveredScreens/storeZoneDeliveredScreen.dart';
import 'package:ondunova_app/Styles/Screens/DeliveredScreens/detailsOfDeliveredScreenStyles.dart';

// import 'package:ondunova_app/Api/apiProvider.dart';
// import 'package:ondunova_app/Constants/enums.dart';

import 'package:ondunova_app/utils/materialColorFromHex.dart';
import 'package:ondunova_app/utils/ordersActions.dart';

class DetailsOfDeliveredScreen extends StatefulWidget {
  final void Function(int, Order) returnPalet;
  final void Function(int) changeTab;
  final OrderJob orderJob;

  DetailsOfDeliveredScreen(
      {Key key, this.returnPalet, this.changeTab, @required this.orderJob})
      : super(key: key);
  @override
  _DetailsOfDeliveredScreenState createState() =>
      _DetailsOfDeliveredScreenState();
}

class _DetailsOfDeliveredScreenState extends State<DetailsOfDeliveredScreen> {
  OrderJob orderJob;
  ApiProvider _apiProvider = ApiProvider();
  OrderActions _orderActions = OrderActions();

  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      orderJob = widget.orderJob;
    });
  }

  getCardColor(orderJobDisplayStatus) {
    var cardColor;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      cardColor = Colors.blue[600];
    } else if (describeEnum(DepositState.C0) == orderJobDisplayStatus.trim()) {
      cardColor = Colors.blue[200];
    } else {
      cardColor = Colors.amber[200];
    }

    return cardColor;
  }

  getOrderStatus(orderStatus) {
    var orderStatusName;
    if (orderStatus == describeEnum(OrderStatus.EnRecogida)) {
      orderStatusName = OrderStatus.EnRecogida.name;
    } else if (orderStatus == describeEnum(OrderStatus.EnTransito)) {
      orderStatusName = OrderStatus.EnTransito.name;
    } else if (orderStatus == describeEnum(OrderStatus.EnAlmacen)) {
      orderStatusName = OrderStatus.EnAlmacen.name;
    } else {
      orderStatusName = OrderStatus.EnProduccion.name;
    }

    return orderStatusName;
  }

  getNumOfStyle(orderJobDisplayStatus) {
    var cardColor = numOfItemTextStyle;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      cardColor = numOfItemC1TextStyle;
    }
    return cardColor;
  }

  getTitleStyle(orderJobDisplayStatus) {
    var cardColor = titleTextStyle;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      cardColor = titleC1TextStyle;
    }
    return cardColor;
  }

  getSubtitleStyle(orderJobDisplayStatus) {
    var style = subTitleTextStyle;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      style = subTitleC1TextStyle;
    }
    return style;
  }

  getDetailStyle(orderJobDisplayStatus) {
    var style = detailOrderTextStyle;
    if (describeEnum(DepositState.C1) == orderJobDisplayStatus.trim()) {
      style = detailC1OrderTextStyle;
    }
    return style;
  }

  returnToStore(Order order) {
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => StoreZoneDeliveredScreen(order: order)))
        .then((value) => getOrders());
  }

  getOrders() async {
    setState(() {
      isLoading = true;
    });
    ApiResponse orderResponse =
        await _apiProvider.getOrdersByOf(orderJob.orderNumber.trim());
    List<Order> orders = _orderActions.getOrdersFromResponse(orderResponse);
    orders =
        _orderActions.filterByOrderStatus(orders, OrderStatus.EnProduccion);

    setState(() {
      isLoading = false;
      orderJob.orders = orders;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: createMaterialColor(Color(0xFF00644b)),
            centerTitle: true,
            title: Text('Detall Odre de Fabricació', style: appBarTextStyle),
            iconTheme: IconThemeData(
              color: Color(0xFFb7ce00),
            ),
          ),
          body: Center(
            child: CircularProgressIndicator(),
          ));
    } else if (orderJob.orders.length > 0) {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: createMaterialColor(Color(0xFF00644b)),
            centerTitle: true,
            title: Text('Detall Odre de Fabricació', style: appBarTextStyle),
            iconTheme: IconThemeData(
              color: Color(0xFFb7ce00), //change your color here
            ),
          ),
          body: ListView(
            children: List.generate(orderJob.orders.length, (index) {
              var depositDate = "";
              var cardColor = getCardColor(orderJob.displayStatus);
              var orderStatus =
                  getOrderStatus(orderJob.orders[index].orderStatus);

              var numOfStyle = getNumOfStyle(orderJob.displayStatus);
              var titleStyle = getTitleStyle(orderJob.displayStatus);
              var subTitleStyle = getSubtitleStyle(orderJob.displayStatus);
              var detailOrderStyle = getDetailStyle(orderJob.displayStatus);

              if (orderJob.orders[index].pickupDate != null) {
                depositDate = DateFormat('yyyy/MM/dd HH:mm:ss')
                    .format(DateTime.parse(orderJob.orders[index].depositDate));
              }

              return Container(
                  margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  decoration: BoxDecoration(color: cardColor),
                  child: Column(children: [
                    ListTile(
                        tileColor: cardColor,
                        leading: Text(orderJob.orders[index].numOf,
                            style: numOfStyle),
                        title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(orderJob.orders[index].machineName,
                                  style: titleStyle),
                              Text(
                                  "Palet " +
                                      orderJob.orders[index].numPalet
                                          .toString(),
                                  style: titleStyle),
                            ]),
                        subtitle: Text(depositDate, style: subTitleStyle),
                        trailing:
                            Row(mainAxisSize: MainAxisSize.min, children: [
                          DeliveredButton(
                            onPressed: () {
                              returnToStore(orderJob.orders[index]);
                              /* _showDialog(orderJob.orders[index]); */
                            },
                          )
                        ])),
                    Divider(),
                    Padding(
                      padding: EdgeInsets.all(15),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    'Quantitat: ${orderJob.orders[index].quantity}',
                                    style: detailOrderStyle,
                                  ),
                                  Text(
                                    'Client: ${orderJob.orders[index].clientName.trim()}',
                                    style: detailOrderStyle,
                                  ),
                                  Text(
                                      'Total palets: ${orderJob.noPallets.toString()}',
                                      style: detailOrderStyle)
                                ]),
                          ]),
                    ),
                  ]));
            }),
          ));
    } else {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: createMaterialColor(Color(0xFF00644b)),
            centerTitle: true,
            title: Text('Detall Odre de Fabricació', style: appBarTextStyle),
            iconTheme: IconThemeData(
              color: Color(0xFFb7ce00), //change your color here
            ),
          ),
          body: Center(
            child: Text(
              "No hi ha palets en màquina per la OF nº ${orderJob.orderNumber}",
              style: detailOrderTextStyle,
            ),
          ));
    }
  }
}

class DeliveredButton extends StatelessWidget {
  DeliveredButton({this.onPressed});

  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Text("Devolució", style: returnButtonTextStyle),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: 200,
        height: 60,
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      fillColor: createMaterialColor(Color(0xFF00644b)),
    );
  }
}
