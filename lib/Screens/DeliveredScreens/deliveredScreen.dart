import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import "package:collection/collection.dart";
import 'package:ondunova_app/Class/apiResponseClass.dart';
import 'package:ondunova_app/utils/ordersActions.dart';
import 'package:ondunova_app/utils/ordersJobsActions.dart';
import 'package:swipedetector/swipedetector.dart';
//Screens
import 'package:ondunova_app/Screens/DeliveredScreens/detailsOfDeliveredScreen.dart';
//Api
import 'package:ondunova_app/Api/apiProvider.dart';
//Class
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';
import 'package:ondunova_app/Class/groupedOrderJobsClass.dart';
//Constants
import 'package:ondunova_app/Constants/Enums/machineCodesEnum.dart';
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';
//Styles
import 'package:ondunova_app/Styles/Screens/DeliveredScreens/deliveredScreenStyles.dart';
//Utils
import 'package:ondunova_app/utils/materialColorFromHex.dart';
//Components
import 'package:ondunova_app/Components/machineList.dart';

class DeliveredScreen extends StatefulWidget {
  final void Function(int, Order) returnPalet;
  final void Function(int) changeTab;
  DeliveredScreen({Key key, this.returnPalet, @required this.changeTab})
      : super(key: key);

  @override
  _DeliveredScreenState createState() => _DeliveredScreenState();
}

class _DeliveredScreenState extends State<DeliveredScreen> {
  Future<List<OrderJob>> _futureOrderJobs;
  Future<bool> _futureOrderDelivered;

  ApiProvider _apiProvider = ApiProvider();
  OrderActions _orderActions = OrderActions();
  OrderJobsActions _orderJobsActions = OrderJobsActions();

  List<bool> indexDropdown;
  List<OrderJob> orderJobs;

  ApiResponse orderJobsResponse;
  ApiResponse orderResponse;

  bool isLoading = true;

  //List of orderJobs by machine
  GroupedOrderJobs curioniOrderJobs;
  GroupedOrderJobs martinOrderJobs;
  GroupedOrderJobs gopfertOrderJobs;
  GroupedOrderJobs serraOrderJobs;
  GroupedOrderJobs rtaOrderJobs;
  GroupedOrderJobs rapidexOrderJobs;

  void initState() {
    super.initState();
    getOrderJobs();
  }

  getOrderJobs() async {
    setState(() {
      isLoading = true;
    });

    orderJobsResponse = await _apiProvider.getOrderJobs();
    orderResponse = await _apiProvider.getOrders();

    if (orderJobsResponse.success && orderResponse.success) {
      List<OrderJob> currentOrderJobs =
          _orderJobsActions.getOrdersJobsFromResponse(orderJobsResponse);
      List<Order> orders = _orderActions.getOrdersFromResponse(orderResponse);
      currentOrderJobs = _orderJobsActions.mergeOrdersToOrderJobs(
          currentOrderJobs, orders, OrderStatus.EnProduccion);
      await setOrderJobs(currentOrderJobs);

      setState(() {
        orderJobs = currentOrderJobs;
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  /*  getOrderJobs() async {
    this.setState(() {
      isLoading = true;
    });

    ApiResponse orderJobsResponse = await _apiProvider.getOrderJobs();
    Iterable oj = orderJobsResponse.data;
    List<OrderJob> orderJobs =
        List<OrderJob>.from(oj.map((model) => OrderJob.fromJson(model)));

    ApiResponse ordersResponse = await _apiProvider.getOrders();
    Iterable o = ordersResponse.data;
    List<Order> orders =
        List<Order>.from(o.map((model) => Order.fromJson(model)));

    if (orders.length > 0) {
      orders.sort((a, b) => a.numPalet.compareTo(b.numPalet));
      var orderIndex = 0;
      for (final orderJob in orderJobs) {
        for (final order in orders) {
          if (order.orderStatus == describeEnum(OrderStatus.EnProduccion) &&
              order.numOf.trim() == orderJob.orderNumber.trim()) {
            orderJob.orders.insert(orderIndex, order);
            orderIndex++;
          }
        }
        orderIndex = 0;
      }
    }
    setOrderJobs(orderJobs);

    setState(() {
      _futureOrderJobs = _apiProvider
          .getOrderJobs(OrderStatus.EnProduccion)
          .then((orderJobs) => setOrderJobs(orderJobs));
    });
  } */

  setOrderJobs(orderJobsResponse) async {
    var mapedOrderJobs;

    mapedOrderJobs =
        groupBy(orderJobsResponse, (OrderJob order) => order.machineCode);

    mapedOrderJobs.forEach((group, orderJobs) {
      orderJobs.removeRange(3, orderJobs.length);
      if (group.trim() == MachineTypes.Curioni.code) {
        curioniOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.Martin.code) {
        martinOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.Gopfert.code) {
        gopfertOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.Serra.code) {
        serraOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.RTA.code) {
        rtaOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      } else if (group.trim() == MachineTypes.Rapidex.code) {
        rapidexOrderJobs = GroupedOrderJobs(group.trim(), orderJobs);
      }
    });
  }

  deliveredAction(Order order) {
    widget.returnPalet(1, order);
  }

  setOrderDeliveredSucces(succes) {
    if (succes) {
      getOrderJobs();
    }

    return succes;
  }

  goToDetailOrderJob(OrderJob orderJob) {
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    DetailsOfDeliveredScreen(orderJob: orderJob)))
        .then((value) => getOrderJobs());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: createMaterialColor(Color(0xFF00644b)),
          centerTitle: true,
          title: Text('Devolució / Retirada', style: appBarTextStyle),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    getOrderJobs();
                  },
                  child:
                      Icon(Icons.refresh, size: 30.0, color: Color(0xFFb7ce00)),
                )),
          ]),
      body: SwipeDetector(
          onSwipeLeft: () {
            widget.changeTab(4);
          },
          onSwipeRight: () {
            widget.changeTab(2);
          },
          child: Center(
              child: (isLoading)
                  ? Center(
                      child: SizedBox(
                          height: 30,
                          width: 30,
                          child: CircularProgressIndicator()))
                  : (!orderJobsResponse.success)
                      ? Center(
                          child: Icon(
                          Icons.error_outline,
                          color: Colors.red,
                          size: iconSize,
                        ))
                      : Container(
                          child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                child: MachineList(
                              listOrderJob: curioniOrderJobs.orderJobs,
                              openDetail: (orderJob) {
                                goToDetailOrderJob(orderJob);
                              },
                              title: 'Curioni',
                            )),
                            Expanded(
                                child: MachineList(
                              listOrderJob: rapidexOrderJobs.orderJobs,
                              openDetail: (orderJob) {
                                goToDetailOrderJob(orderJob);
                              },
                              title: 'Rapidex',
                            )),
                            Expanded(
                                child: MachineList(
                              listOrderJob: rtaOrderJobs.orderJobs,
                              openDetail: (orderJob) {
                                goToDetailOrderJob(orderJob);
                              },
                              title: 'RTA',
                            )),
                            Expanded(
                                child: MachineList(
                              listOrderJob: serraOrderJobs.orderJobs,
                              openDetail: (orderJob) {
                                goToDetailOrderJob(orderJob);
                              },
                              title: 'Serra',
                            )),
                            Expanded(
                                child: MachineList(
                              listOrderJob: martinOrderJobs.orderJobs,
                              openDetail: (orderJob) {
                                goToDetailOrderJob(orderJob);
                              },
                              title: 'Martin',
                            )),
                            Expanded(
                                child: MachineList(
                              listOrderJob: gopfertOrderJobs.orderJobs,
                              openDetail: (orderJob) {
                                goToDetailOrderJob(orderJob);
                              },
                              title: 'Gopfert',
                            )),
                          ],
                        )))),
    );
  }
}
