import 'package:flutter/material.dart';

const numOfItemTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 32,
  fontWeight: FontWeight.w700,
);

const titleTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 32,
  fontWeight: FontWeight.w500,
);

const subTitleTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 30,
  fontWeight: FontWeight.w400,
);

const zoneButtonTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 30,
  fontWeight: FontWeight.w700,
);

//SizeBox
const double storeZoneSizedBoxWidth = 80;

//StoreButton
const double storeZoneButtonWidth = 240;
const double storeZoneButtonHeight = 100;
const double storeZoneBorderRadius = 20;
