import 'package:flutter/material.dart';

const dataTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 14,
  fontWeight: FontWeight.w400,
);

const dataC1TextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 14,
  fontWeight: FontWeight.w400,
);

const titleTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 20,
  fontWeight: FontWeight.w800,
);

const titleC1TextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 20,
  fontWeight: FontWeight.w800,
);
