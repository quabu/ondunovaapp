import 'package:flutter/material.dart';

//Text
const numPadNumberTextStyle = TextStyle(
  fontSize: 30,
  fontWeight: FontWeight.w400,
);

//Dimensions
const double sizeBoxWidth = 15;
const double sizeBoxHeight = 15;

const double sizeTextBoxHeight = 15;

const double buttonHeight = 80;
const double buttonWidth = 80;
