import 'package:flutter/material.dart';

const validationButtonTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 22,
  fontWeight: FontWeight.w700,
);

const double validationButtonHeight = 60;
const double validationButtonWidth = 260;
const double validationbBorderRadius = 30;
