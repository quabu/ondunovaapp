import 'package:flutter/material.dart';

//Text
const appBarTextStyle = TextStyle(
    fontWeight: FontWeight.bold, color: Color(0xFFe6e6e6), fontSize: 28);

const numOfItemTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 30,
  fontWeight: FontWeight.w500,
);

const titleTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 26,
  fontWeight: FontWeight.w500,
);

const subTitleTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 22,
  fontWeight: FontWeight.w500,
);

const pickupButtonTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 22,
  fontWeight: FontWeight.w400,
);

const detailOrderTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 18,
  fontWeight: FontWeight.w400,
);

const messageTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 26,
  fontWeight: FontWeight.w500,
);

//Icon
const double iconSize = 40;
