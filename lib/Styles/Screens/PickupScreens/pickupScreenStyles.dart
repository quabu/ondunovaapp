import 'package:flutter/material.dart';

//Text
const appBarTextStyle = TextStyle(
    fontWeight: FontWeight.bold, color: Color(0xFFe6e6e6), fontSize: 28);

const validationButtonTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 18,
  fontWeight: FontWeight.w700,
);

const inputTitleTextStyle = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w600,
);

const inputValueTextStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.w400,
);

const zoneButtonTextStyle = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w700,
);

const titleStoreTextStyle = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w600,
);

//Padding
const formPadding =
    const EdgeInsets.only(bottom: 10, left: 0, right: 0, top: 20);
const textInputPadding = EdgeInsets.only(bottom: 0, left: 20, right: 0, top: 0);
const titleStorePadding =
    EdgeInsets.only(bottom: 15, left: 0, right: 0, top: 20);

const sendButtonPadding =
    EdgeInsets.only(bottom: 10, left: 0, right: 0, top: 20);
//Dimensions
//SizedBox
const double widthSizedBoxValidation = 20;
const double generalHeightSizedBox = 15;
const double inputWidthSizedBox = 20;

const double progressIndicatorSizedBoxHeight = 5;
const double progressIndicatorSizedBoxWidth = 400;

const double iconSizeBoxHeight = 60;
const double iconSizeBoxWidth = 400;

const double storeZoneSizedBoxWidth = 15;
//Input
const double textInputHeight = 60;
const double numOfInputWidth = 450;
const double inputWidth = 240;
const double inputBorderRadius = 30;

//ValidationButton
const double validationButtonHeight = 60;
const double validationButtonWidth = 180;
const double validationbBorderRadius = 30;

//StoreButton
const double storeZoneButtonWidth = 70;
const double storeZoneButtonHeight = 80;
const double storeZoneBorderRadius = 15;

//Icon
const double iconSize = 40;
