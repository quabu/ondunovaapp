import 'package:flutter/material.dart';

//Text
const appBarTextStyle = TextStyle(
    fontWeight: FontWeight.bold, color: Color(0xFFe6e6e6), fontSize: 28);

//Icon
const double iconSize = 40;
