import 'package:flutter/material.dart';

//Text
const appBarTextStyle = TextStyle(
    color: Color(0xFFe6e6e6), fontWeight: FontWeight.bold, fontSize: 28);

const subTitleTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 18,
  fontWeight: FontWeight.w400,
);

const depositButtonTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 20,
  fontWeight: FontWeight.w700,
);

const infoOrderTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 18,
  fontWeight: FontWeight.w500,
);

const titleStoreTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 22,
  fontWeight: FontWeight.w600,
);

//Padding
const formPadding =
    const EdgeInsets.only(bottom: 10, left: 0, right: 0, top: 20);
const textInputPadding = EdgeInsets.only(bottom: 0, left: 20, right: 0, top: 0);
const titleStorePadding =
    EdgeInsets.only(bottom: 15, left: 0, right: 0, top: 20);

const sendButtonPadding =
    EdgeInsets.only(bottom: 10, left: 0, right: 0, top: 20);

const infoOrderPadding =
    const EdgeInsets.only(bottom: 0, left: 30, right: 0, top: 0);

const numPaletsPadding =
    const EdgeInsets.only(bottom: 0, left: 0, right: 8, top: 0);
//Dimensions
//SizedBox
const double widthSizedBoxValidation = 20;
const double generalHeightSizedBox = 15;
const double inputWidthSizedBox = 20;

const double progressIndicatorSizedBoxHeight = 5;
const double progressIndicatorSizedBoxWidth = 600;

const double iconSizeBoxHeight = 60;
const double iconSizeBoxWidth = 400;

const double storeZoneSizedBoxWidth = 15;
//Input
const double textInputHeight = 70;
const double numOfInputWidth = 600;
const double inputWidth = 340;
const double inputBorderRadius = 35;

//ValidationButton
const double validationButtonHeight = 70;
const double validationButtonWidth = 180;
const double validationbBorderRadius = 35;

//StoreButton
const double storeZoneButtonWidth = 100;
const double storeZoneButtonHeight = 120;
const double storeZoneBorderRadius = 20;

//Icon
const double iconSize = 40;
