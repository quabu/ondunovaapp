import 'package:flutter/material.dart';

//Text
const appBarTextStyle = TextStyle(
    color: Color(0xFFe6e6e6), fontWeight: FontWeight.bold, fontSize: 28);

const numOfItemTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 30,
  fontWeight: FontWeight.w700,
);

const numOfItemC1TextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 30,
  fontWeight: FontWeight.w700,
);

const titleTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 24,
  fontWeight: FontWeight.w600,
);

const titleC1TextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 24,
  fontWeight: FontWeight.w600,
);

const subTitleTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 18,
  fontWeight: FontWeight.w400,
);

const subTitleC1TextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 18,
  fontWeight: FontWeight.w400,
);

const infoButtonTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 22,
  fontWeight: FontWeight.w700,
);

const detailOrderTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 18,
  fontWeight: FontWeight.w400,
);

const detailC1OrderTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 18,
  fontWeight: FontWeight.w400,
);

const titleStoreTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 18,
  fontWeight: FontWeight.w600,
);

const returnButtonTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 22,
  fontWeight: FontWeight.w700,
);
