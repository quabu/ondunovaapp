import 'package:flutter/material.dart';

//Text
const appBarTextStyle = TextStyle(
    color: Color(0xFFe6e6e6), fontWeight: FontWeight.bold, fontSize: 28);

//Icon
const double iconSize = 40;
