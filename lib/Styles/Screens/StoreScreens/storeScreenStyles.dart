import 'package:flutter/material.dart';

//Text
const appBarTextStyle = TextStyle(
    color: Color(0xFFe6e6e6), fontWeight: FontWeight.bold, fontSize: 28);

const numOfItemTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 30,
  fontWeight: FontWeight.w700,
);

const titleTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 24,
  fontWeight: FontWeight.w600,
);

const subTitleTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 18,
  fontWeight: FontWeight.w400,
);

const returnButtonTextStyle = TextStyle(
  color: Color(0xFFe6e6e6),
  fontSize: 22,
  fontWeight: FontWeight.w700,
);

const titleOrderTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 24,
  fontWeight: FontWeight.w600,
);

const infoOrderTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 18,
  fontWeight: FontWeight.w500,
);

const titleStoreTextStyle = TextStyle(
  color: Color(0xFF00644b),
  fontSize: 22,
  fontWeight: FontWeight.w600,
);

const zoneButtonTextStyle = TextStyle(
  color: Color(0xFFb7ce00),
  fontSize: 18,
  fontWeight: FontWeight.w500,
);

//Padding
const formPadding =
    const EdgeInsets.only(bottom: 10, left: 0, right: 0, top: 20);
const textInputPadding = EdgeInsets.only(bottom: 0, left: 20, right: 0, top: 0);
const titleStorePadding =
    EdgeInsets.only(bottom: 15, left: 0, right: 0, top: 20);

const sendButtonPadding =
    EdgeInsets.only(bottom: 10, left: 0, right: 0, top: 20);

const infoOrderPadding =
    const EdgeInsets.only(bottom: 0, left: 30, right: 0, top: 0);

const numPaletsPadding =
    const EdgeInsets.only(bottom: 0, left: 0, right: 16, top: 0);

const orderButtonsPadding =
    const EdgeInsets.only(bottom: 0, left: 0, right: 10, top: 0);
//Dimensions
//SizedBox
//
////Icon
const double iconSize = 40;
