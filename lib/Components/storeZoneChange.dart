import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
//Components
import 'package:ondunova_app/Components/generalButton.dart';
//Class
import 'package:ondunova_app/Class/orderClass.dart';
//Enums
import 'package:ondunova_app/Constants/Enums/storeZoneEnum.dart';
//Styles
import 'package:ondunova_app/Styles/Components/storeZoneChangeStyles.dart';

class StoreZoneChange extends StatelessWidget {
  StoreZoneChange(
      {@required this.order,
      @required this.zoneSelected,
      @required this.setStoreZone,
      @required this.actionZone});

  final Order order;
  final StoreZoneType zoneSelected;
  final Function(StoreZoneType, String) setStoreZone;
  final Function() actionZone;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
          elevation: 2,
          color: Color(0xFFe6e6e6),
          margin: EdgeInsets.all(15),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Expanded(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(order.numOf, style: numOfItemTextStyle),
                SizedBox(
                  width: 30,
                ),
                Text(order.machineName, style: titleTextStyle),
                SizedBox(
                  width: 30,
                ),
                Text('Número de palet: ${order.numPalet}',
                    style: subTitleTextStyle),
              ],
            )),
            Divider(),
            Container(
              child: Column(
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      StoreZone(
                        onPressed: () {
                          setStoreZone(
                              StoreZoneType.Zone1, StoreZoneType.Zone1.name);
                        },
                        zone: StoreZoneType.Zone1.name,
                        zoneSelected: zoneSelected,
                        zoneType: StoreZoneType.Zone1,
                      ),
                      SizedBox(width: storeZoneSizedBoxWidth),
                      StoreZone(
                        onPressed: () {
                          setStoreZone(
                              StoreZoneType.Zone2, StoreZoneType.Zone2.name);
                        },
                        zone: StoreZoneType.Zone2.name,
                        zoneSelected: zoneSelected,
                        zoneType: StoreZoneType.Zone2,
                      ),
                      SizedBox(width: storeZoneSizedBoxWidth),
                      StoreZone(
                        onPressed: () {
                          setStoreZone(
                              StoreZoneType.Zone3, StoreZoneType.Zone3.name);
                        },
                        zone: StoreZoneType.Zone3.name,
                        zoneSelected: zoneSelected,
                        zoneType: StoreZoneType.Zone3,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      StoreZone(
                        onPressed: () {
                          setStoreZone(
                              StoreZoneType.Zone4, StoreZoneType.Zone4.name);
                        },
                        zone: StoreZoneType.Zone4.name,
                        zoneSelected: zoneSelected,
                        zoneType: StoreZoneType.Zone4,
                      ),
                      SizedBox(width: storeZoneSizedBoxWidth),
                      StoreZone(
                        onPressed: () {
                          setStoreZone(
                              StoreZoneType.Zone5, StoreZoneType.Zone5.name);
                        },
                        zone: StoreZoneType.Zone5.name,
                        zoneSelected: zoneSelected,
                        zoneType: StoreZoneType.Zone5,
                      ),
                      SizedBox(width: storeZoneSizedBoxWidth),
                      StoreZone(
                        onPressed: () {
                          setStoreZone(
                              StoreZoneType.Zone6, StoreZoneType.Zone6.name);
                        },
                        zone: StoreZoneType.Zone6.name,
                        zoneSelected: zoneSelected,
                        zoneType: StoreZoneType.Zone6,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GeneralButton(
                          onPressed: () async {
                            actionZone();
                          },
                          color: Color(0xFF00644b),
                          buttonText: 'Acceptar'),
                    ],
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            )
          ])),
    );
  }
}

class StoreZone extends StatelessWidget {
  StoreZone(
      {@required this.zone,
      @required this.onPressed,
      @required this.zoneType,
      @required this.zoneSelected});

  final String zone;
  final Function onPressed;
  final StoreZoneType zoneType;
  final StoreZoneType zoneSelected;

  @override
  Widget build(BuildContext context) {
    return StoreZoneButton(
      onPressed: this.onPressed,
      zoneSelected: (zoneSelected == zoneType) ? true : false,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("$zone", style: zoneButtonTextStyle),
          ]),
    );
  }
}

class StoreZoneButton extends StatelessWidget {
  StoreZoneButton(
      {@required this.child,
      @required this.onPressed,
      @required this.zoneSelected});

  final Widget child;
  final Function onPressed;
  final bool zoneSelected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onPressed,
        child: SizedBox(
            height: storeZoneButtonHeight,
            width: storeZoneButtonWidth,
            child: Container(
                decoration: BoxDecoration(
                    color: Color(0xFFFFFFFF),
                    border: zoneSelected
                        ? Border.all(color: Color(0xFFb7ce00), width: 4)
                        : Border.all(color: Color(0xFF00644b), width: 2),
                    borderRadius:
                        new BorderRadius.circular(storeZoneBorderRadius)),
                child: child)));
  }
}
