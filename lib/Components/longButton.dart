import 'package:flutter/material.dart';

//Utils
import 'package:ondunova_app/utils/materialColorFromHex.dart';
//Styles
import 'package:ondunova_app/Styles/Components/longButtonStyles.dart';

class LongButton extends StatelessWidget {
  LongButton({@required this.onPressed, this.buttonText, @required this.color});

  final Function onPressed;
  final String buttonText;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Text("$buttonText",
          textAlign: TextAlign.left,
          maxLines: 1,
          style: validationButtonTextStyle),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: validationButtonWidth,
        height: validationButtonHeight,
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(validationbBorderRadius),
          side: BorderSide(color: createMaterialColor(color), width: 1)),
      fillColor: createMaterialColor(color),
    );
  }
}
