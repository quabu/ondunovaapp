import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ondunova_app/utils/materialColorFromHex.dart';

class CustomAlertDialog extends StatelessWidget {
  CustomAlertDialog({
    @required this.title,
    @required this.subtitle,
    @required this.validateText,
    @required this.cancelText,
    @required this.onValidationPressed,
    @required this.onRejectPressed,
  });

  final String title;
  final String subtitle;
  final String validateText;
  final String cancelText;
  final Function onRejectPressed;
  final Function onValidationPressed;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text("$title"),
        content: Text("$subtitle"),
        elevation: 3,
        actionsPadding: EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 5),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        actions: <Widget>[
          DialogButton(
              onPressed: onValidationPressed, textButton: validateText),
          DialogButton(onPressed: onRejectPressed, textButton: cancelText)
        ]);
  }
}

class DialogButton extends StatelessWidget {
  DialogButton({@required this.onPressed, @required this.textButton});

  final Function onPressed;
  final String textButton;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Text(
        "$textButton",
        style: TextStyle(
          color: Color(0xFFe6e6e6),
          fontSize: 18,
          fontWeight: FontWeight.w700,
        ),
      ),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: 120,
        height: 40,
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      fillColor: createMaterialColor(Color(0xFF00644b)),
    );
  }
}
