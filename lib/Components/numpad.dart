import 'package:flutter/material.dart';
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Styles/Components/numpadStyles.dart';
import 'package:ondunova_app/utils/materialColorFromHex.dart';

import '../main.dart';

class Numpad extends StatefulWidget {
  final void Function(String) setInputValue;
  final String currentValue;
  Numpad({@required this.setInputValue, @required this.currentValue});

  @override
  _NumpadState createState() => _NumpadState();
}

class _NumpadState extends State<Numpad> {
  String typedNumber = "";
  int inputIndex = 0;
  List<String> inputValues = [];

  @override
  void initState() {
    setState(() {
      typedNumber = widget.currentValue;
    });
    super.initState();
  }

  @override
  void didUpdateWidget(Numpad oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.currentValue != oldWidget.currentValue) {
      setState(() {
        typedNumber = widget.currentValue;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Padding(
          padding:
              const EdgeInsets.only(bottom: 10, left: 20, right: 0, top: 40),
          child: Column(children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                NumberedRoundButton(
                    num: "1",
                    onPressed: () {
                      setState(() {
                        typedNumber += "1";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                ),
                NumberedRoundButton(
                    num: "2",
                    onPressed: () {
                      setState(() {
                        typedNumber += "2";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                ),
                NumberedRoundButton(
                    num: "3",
                    onPressed: () {
                      setState(() {
                        typedNumber += "3";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                )
              ],
            ),
            SizedBox(
              height: sizeBoxHeight,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                NumberedRoundButton(
                    num: "4",
                    onPressed: () {
                      setState(() {
                        typedNumber += "4";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                ),
                NumberedRoundButton(
                    num: "5",
                    onPressed: () {
                      setState(() {
                        typedNumber += "5";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                ),
                NumberedRoundButton(
                    num: "6",
                    onPressed: () {
                      setState(() {
                        typedNumber += "6";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                )
              ],
            ),
            SizedBox(
              height: sizeBoxHeight,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                NumberedRoundButton(
                    num: "7",
                    onPressed: () {
                      setState(() {
                        typedNumber += "7";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                ),
                NumberedRoundButton(
                    num: "8",
                    onPressed: () {
                      setState(() {
                        typedNumber += "8";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                ),
                NumberedRoundButton(
                    num: "9",
                    onPressed: () {
                      setState(() {
                        typedNumber += "9";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                )
              ],
            ),
            SizedBox(
              height: sizeBoxHeight,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                NumberedRoundButton(
                    num: 'a',
                    onPressed: () {
                      setState(() {
                        typedNumber += "a";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                ),
                NumberedRoundButton(
                    num: "0",
                    onPressed: () {
                      setState(() {
                        typedNumber += "0";
                      });
                      widget.setInputValue(typedNumber);
                    }),
                SizedBox(
                  width: sizeBoxWidth,
                ),
                DeleteButton(
                  onPressed: () {
                    setState(() {
                      typedNumber =
                          typedNumber.substring(0, typedNumber.length - 1);
                    });
                    widget.setInputValue(typedNumber);
                  },
                ),
                SizedBox(
                  width: sizeBoxWidth,
                ),
              ],
            ),
            SizedBox(
              height: sizeBoxHeight,
            ),
          ])),
    ]);
  }
}

class NumberedRoundButton extends StatelessWidget {
  NumberedRoundButton({this.num, this.onPressed});

  final String num;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RoundButton(
      onPressed: this.onPressed,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("$num", style: numPadNumberTextStyle),
          ]),
    );
  }
}

class RoundButton extends StatelessWidget {
  RoundButton({@required this.child, @required this.onPressed});

  final Widget child;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: child,
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: buttonWidth,
        height: buttonHeight,
      ),
      shape: CircleBorder(),
      fillColor: Colors.grey.shade300,
    );
  }
}

class DeleteButton extends StatelessWidget {
  DeleteButton({this.onPressed});

  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Icon(
        Icons.backspace,
        size: 40,
        color: createMaterialColor(Color(0xFFb7ce00)),
      ),
      onPressed: onPressed,
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: buttonWidth,
        height: buttonHeight,
      ),
      shape: CircleBorder(),
      fillColor: createMaterialColor(Color(0xFF00644b)),
    );
  }
}
