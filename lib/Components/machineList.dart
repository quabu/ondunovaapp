import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

//Class
import 'package:ondunova_app/Class/orderJobClass.dart';
//Enums
import 'package:ondunova_app/Constants/Enums/enums.dart';
//Styles
import 'package:ondunova_app/Styles/Components/machineListStyles.dart';

class MachineList extends StatelessWidget {
  MachineList(
      {@required this.title,
      @required this.listOrderJob,
      @required this.openDetail});

  final String title;
  final List<OrderJob> listOrderJob;
  final Function(OrderJob) openDetail;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: 10,
        ),
        Text(title,
            style: TextStyle(
              color: Color(0xFF00644b),
              fontSize: 26,
              fontWeight: FontWeight.w600,
            )),
        Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: listOrderJob.length,
                itemBuilder: (context, index) {
                  var dataStyle = dataTextStyle;
                  var titleStyle = titleTextStyle;
                  if (describeEnum(DepositState.C1) ==
                      listOrderJob[index].displayStatus.trim()) {
                    dataStyle = dataC1TextStyle;
                    titleStyle = titleC1TextStyle;
                  }
                  return Card(
                      elevation: 2,
                      color: describeEnum(DepositState.C1) ==
                              listOrderJob[index].displayStatus.trim()
                          ? Colors.blue[600]
                          : describeEnum(DepositState.C0) ==
                                  listOrderJob[index].displayStatus.trim()
                              ? Colors.blue[200]
                              : Colors.amber[200],
                      margin: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          ListTile(
                              onTap: () => openDetail(listOrderJob[index]),
                              title: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        listOrderJob[index]
                                                .orderNumber
                                                .toString()
                                                .trim() +
                                            listOrderJob[index]
                                                .jobNumber
                                                .toString(),
                                        style: titleStyle,
                                        textAlign: TextAlign.left),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                        'Fecha: ' +
                                            DateFormat('dd/MM/yy').format(
                                                DateTime.parse(
                                                    listOrderJob[index]
                                                        .startDate)),
                                        style: dataStyle),
                                    Text(
                                        'Hora: ' +
                                            DateFormat('HH:mm').format(
                                                DateTime.parse(
                                                    listOrderJob[index]
                                                        .startDate)),
                                        style: dataStyle),
                                    Text(
                                        listOrderJob[index]
                                            .clientName
                                            .toString()
                                            .trim(),
                                        style: dataStyle),
                                    Text(
                                        listOrderJob[index]
                                                .orders
                                                .length
                                                .toString() +
                                            ' / ' +
                                            (listOrderJob[index].noPallets -
                                                    listOrderJob[index]
                                                        .orders
                                                        .length)
                                                .toString() +
                                            ' / ' +
                                            listOrderJob[index]
                                                .noPallets
                                                .toString(),
                                        style: dataStyle),
                                  ])),
                        ],
                      ));
                }))
      ],
    );
  }
}
