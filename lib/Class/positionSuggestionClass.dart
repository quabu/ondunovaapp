class PositionSuggestion {
  final String storePosition;

  PositionSuggestion({this.storePosition});

  factory PositionSuggestion.fromJson(Map<dynamic, dynamic> json) {
    return PositionSuggestion(storePosition: json['storePosition']);
  }
}
