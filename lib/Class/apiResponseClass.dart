import 'package:flutter/material.dart';

class ApiResponse {
  final String message;
  final List<dynamic> data;
  final bool success;

  ApiResponse({
    @required this.message,
    @required this.data,
    @required this.success,
  });

  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse(
      message: json['message'],
      data: json['data'],
      success: json['success'],
    );
  }
}
