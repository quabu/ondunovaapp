import 'package:flutter/material.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';

class GroupedOrderJobs {
  final String group;
  final List<OrderJob> orderJobs;

  GroupedOrderJobs(
    this.group,
    this.orderJobs,
  );
}
