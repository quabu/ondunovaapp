import 'package:flutter/material.dart';
import 'orderClass.dart';

class GroupedOrders {
  final String group;
  final List<Order> orders;

  GroupedOrders(
    this.group,
    this.orders,
  );
}
