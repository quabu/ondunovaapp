import 'package:flutter/material.dart';

import 'orderClass.dart';

class OrderJob {
  final String orderNumber;
  final String jobNumber;
  final String quantityOrdered;
  final String quantityToProduce;
  final String quantityPerPallet;
  final int noPallets;
  final String startDate;
  final String displayStatus;
  final String machineName;
  final String machineCode;
  final String clientId;
  final String clientName;
  List<Order> orders;
  bool orderDropdown;

  OrderJob(
      {@required this.orderNumber,
      @required this.jobNumber,
      @required this.quantityOrdered,
      @required this.quantityToProduce,
      @required this.quantityPerPallet,
      @required this.noPallets,
      @required this.startDate,
      @required this.displayStatus,
      @required this.machineName,
      @required this.machineCode,
      @required this.clientId,
      @required this.clientName,
      this.orders,
      this.orderDropdown});

  factory OrderJob.fromJson(Map<String, dynamic> json) {
    return OrderJob(
        orderNumber: json['OrderNumber'],
        jobNumber: json['JobNumber'],
        quantityOrdered: json['QuantityOrdered'],
        quantityToProduce: json['QuantityToProduce'],
        quantityPerPallet: json['QuantityPerPallet'],
        noPallets: json['NoPallets'],
        startDate: json['StartDate'],
        displayStatus: json['DisplayStatus'],
        machineName: json['MachineName'],
        machineCode: json['MachineCode'],
        clientId: json['ClientId'],
        clientName: json['ClientName'],
        orders: [],
        orderDropdown: false);
  }
}
