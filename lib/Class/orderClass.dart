import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';

class Order {
  final int id;
  final String numOf;
  final int numPalet;
  final int totalPalets;
  final int quantity;
  final String storePosition;
  final String orderStatus;
  final String user;
  final String pickupDate;
  final String depositDate;
  final String machineId;
  final String machineName;
  final String clientId;
  final String clientName;
  final String origin;

  Order({
    this.id,
    this.numOf,
    this.numPalet,
    this.totalPalets,
    this.quantity,
    this.storePosition,
    this.orderStatus,
    this.user,
    this.pickupDate,
    this.depositDate,
    this.machineId,
    this.machineName,
    this.clientId,
    this.clientName,
    this.origin,
  });

  factory Order.fromJson(Map<dynamic, dynamic> json) {
    return Order(
        id: json['id'],
        numOf: json['numOf'],
        numPalet: json['numPalet'],
        totalPalets: json['totalPalets'],
        quantity: json['quantity'],
        storePosition: json['storePosition'],
        orderStatus: json['orderStatus'],
        user: json['user'],
        pickupDate: json['pickupDate'],
        depositDate: json['depositDate'],
        machineId: json['machineId'],
        machineName: json['machineName'],
        clientId: json['clientId'],
        clientName: json['clientName'],
        origin: json['origin']);
  }
}
