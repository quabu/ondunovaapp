import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:ondunova_app/Class/apiResponseClass.dart';

//Constants
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';

//Class
import '../Class/orderClass.dart';
import '../Class/orderJobClass.dart';
import '../Class/apiResponseClass.dart';

class ApiProvider {
  //Local
  /* final String _baseUrl = '10.0.2.2:3000'; */
  //Local Home
  /* final String _baseUrl = '192.168.0.103:3000'; */
  //Local Quabu
  /* final String _baseUrl = '10.0.21.11:3000'; */

  //PROD
  final String _baseUrl = '172.16.21.7:5001';

  Future<ApiResponse> getOrders() async {
    var client = new http.Client();
    final response = await client.get(Uri.http(_baseUrl, '/getOrders'));

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }
  }

  Future<ApiResponse> getOrdersByOf(numOf) async {
    var client = new http.Client();
    final response = await client.post(
      Uri.http(_baseUrl, '/getOrdersByOf'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<dynamic, dynamic>{'numOf': numOf}),
    );

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }
  }

  Future<ApiResponse> getOrderJobs() async {
    var client = new http.Client();
    final response = await client.get(Uri.http(_baseUrl, '/getOrderJobs'));

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }

    /*       final extractedData = json.decode(response.body);
      Iterable l = extractedData['orderJobs'];
      List<Order> orders = await getOrders(null);
      if (orders.length > 0) {
        orders.sort((a, b) => a.numPalet.compareTo(b.numPalet));
        List<OrderJob> orderJobs =
            List<OrderJob>.from(l.map((model) => OrderJob.fromJson(model)));
        var orderIndex = 0;
        for (final orderJob in orderJobs) {
          for (final order in orders) {
            if (order.orderStatus == describeEnum(orderStatus) &&
                order.numOf.trim() == orderJob.orderNumber.trim()) {
              orderJob.orders.insert(orderIndex, order);
              orderIndex++;
            }
          }
          orderIndex = 0;
        }
        return orderJobs;
      } else {
        List<OrderJob> orderJobs =
            List<OrderJob>.from(l.map((model) => OrderJob.fromJson(model)));
        return orderJobs;
      } */
  }

  Future<ApiResponse> deleteOrder(int orderId) async {
    var client = new http.Client();
    final response = await client.post(
      Uri.http(_baseUrl, '/deleteOrder'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<dynamic, dynamic>{'orderId': orderId}),
    );

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }
  }

  Future<ApiResponse> validateOrder(order, jobNumber) async {
    var client = new http.Client();
    final response = await client.post(
      Uri.http(_baseUrl, '/validateOrder'),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
      body:
          jsonEncode(<String, String>{'order': order, 'jobNumber': jobNumber}),
    );
    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }
    /* if (response.statusCode == 200) {
      final extractedData = json.decode(response.body);
      Iterable l = extractedData['order'];
      if (l != null) {
        List<OrderJob> orderJobs =
            List<OrderJob>.from(l.map((model) => OrderJob.fromJson(model)));
        OrderJob orderJob = orderJobs[0];
        Order order = new Order(
            numOf: orderJob.orderNumber + orderJob.jobNumber,
            totalPalets: orderJob.noPallets,
            machineId: orderJob.machineCode,
            machineName: orderJob.machineName,
            clientId: orderJob.clientId,
            clientName: orderJob.clientName,
            origin: "Onduladora");
        return order;
      } else {
        return null;
      }
    } else {
      throw Exception('Failed to load order');
    } */
  }

  //Pickup
  Future<ApiResponse> newOrderPickup(Order order) async {
    var client = new http.Client();
    final response = await client.post(
      Uri.http(_baseUrl, '/newOrderPickup'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<dynamic, dynamic>{
        'orderPickup': {
          'numOf': order.numOf,
          'numPalet': order.numPalet,
          'totalPalets': order.totalPalets,
          'quantity': order.quantity,
          'storePosition': order.storePosition,
          'machineId': order.machineId,
          'machineName': order.machineName,
          'clientId': order.clientId,
          'clientName': order.clientName,
          'origin': order.origin
        }
      }),
    );

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }

    /* if (response.statusCode == 200) {
      final extractedData = json.decode(response.body);
      var success = extractedData['success'];
      return success;
    } else if (response.statusCode == 400) {
      return false;
    } else {
      throw Exception('Failed to load order');
    } */
  }

  Future<ApiResponse> updateOrderStatusById(orderId) async {
    var client = new http.Client();
    final response = await client.post(
      Uri.http(_baseUrl, '/updateOrderStatusById'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
          <dynamic, dynamic>{'orderStatus': "EnTransito", 'orderId': orderId}),
    );

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }
  }

  Future<ApiResponse> updateOrderPickup(Order order) async {
    var client = new http.Client();
    final response = await client.post(
      Uri.http(_baseUrl, '/updateOrderPickup'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<dynamic, dynamic>{
        'orderPickup': {
          'id': order.id,
          'numOf': order.numOf,
          'numPalet': order.numPalet,
          'totalPalets': order.totalPalets,
          'quantity': order.quantity,
          'storePosition': order.storePosition,
          'orderStatus': order.orderStatus,
          'machineId': order.machineId,
          'machineName': order.machineName,
          'clientId': order.clientId,
          'clientName': order.clientName
        }
      }),
    );

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }
  }

  Future<ApiResponse> getPositionSuggestion(String numOf) async {
    var client = new http.Client();
    final response = await client.post(
      Uri.http(_baseUrl, '/getPositionSuggestion'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<dynamic, dynamic>{'numOf': numOf}),
    );

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }

    /*  if (response.statusCode == 200) {
      final extractedData = json.decode(response.body);
      var positionSuggestionObject = extractedData['positionSuggestion'];
      var positionSuggestion = positionSuggestionObject['storePosition'];
      return positionSuggestion;
    } else if (response.statusCode == 400) {
      return "Zona 1";
    } else {
      throw Exception('Failed to load position suggestion');
    } */
  }

  //Deposit
  Future<ApiResponse> newOrderDeposit(Order order) async {
    var client = new http.Client();
    final response = await client.post(
      Uri.http(_baseUrl, '/newOrderDeposit'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<dynamic, dynamic>{
        'orderDeposit': {'id': order.id}
      }),
    );

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }

    /* if (response.statusCode == 200) {
      final extractedData = json.decode(response.body);
      var success = extractedData['success'];
      if (success) {
        Iterable l = extractedData['orders'];
        List<Order> orders =
            List<Order>.from(l.map((model) => Order.fromJson(model)));
        if (order.orderStatus != null) {
          List<Order> filteredOrders = orders
              .where((element) =>
                  element.orderStatus == order.orderStatus &&
                  element.numOf == order.numOf)
              .toList();
          return filteredOrders;
        } else {
          return orders;
        }
      } else {
        return null;
      }
    } else {
      throw Exception('Failed to load order');
    } */
  }

  Future<ApiResponse> newOrderDepositByOf(String numOf) async {
    var client = new http.Client();
    final response = await client.post(
      Uri.http(_baseUrl, '/newOrderDepositByOf'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<dynamic, dynamic>{'numOf': numOf}),
    );

    final extractedData = json.decode(response.body);
    ApiResponse apiResponse = ApiResponse.fromJson(extractedData);

    if (response.statusCode == 200) {
      print(apiResponse.success);
      return apiResponse;
    } else {
      return apiResponse;
    }

    /* if (response.statusCode == 200) {
      final extractedData = json.decode(response.body);
      var success = extractedData['success'];
      return success;
    } else {
      throw Exception('Failed to load order');
    } */
  }
}
