class StoreFilterTypes {
  static const String ByNumOF = 'Número de OF';
  static const String ByZone = 'Zonas';

  static const List<String> choices = <String>[ByNumOF, ByZone];
}
