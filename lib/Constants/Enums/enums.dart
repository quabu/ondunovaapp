//Pickup
enum PickupInputType { NumOf, NumPalet, Quantity }

//Deposit
enum DepositState { C0, C1, B2 }

//Delivered
enum DeliveredState { C0, C1, B2 }
