enum OrderStatus { EnRecogida, EnTransito, EnAlmacen, EnProduccion }

extension OrderStatusExtension on OrderStatus {
  String get status {
    switch (this) {
      case OrderStatus.EnRecogida:
        return 'EnRecogida';
      case OrderStatus.EnTransito:
        return 'EnTransito';
      case OrderStatus.EnAlmacen:
        return 'EnAlmacen';
      case OrderStatus.EnProduccion:
        return 'EnProduccion';
      default:
        return null;
    }
  }

  String get name {
    switch (this) {
      case OrderStatus.EnRecogida:
        return 'En recogida';
      case OrderStatus.EnTransito:
        return 'En transito';
      case OrderStatus.EnAlmacen:
        return 'En almacen';
      case OrderStatus.EnProduccion:
        return 'En producción';
      default:
        return null;
    }
  }
}
