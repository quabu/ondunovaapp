enum MachineTypes { Curioni, Martin, Gopfert, Serra, RTA, Rapidex }

extension MachinesTypesExtension on MachineTypes {
  String get code {
    switch (this) {
      case MachineTypes.Curioni:
        return '2C';
      case MachineTypes.Martin:
        return '2U';
      case MachineTypes.Gopfert:
        return '2R';
      case MachineTypes.Serra:
        return '2J';
      case MachineTypes.RTA:
        return '2S';
      case MachineTypes.Rapidex:
        return '2B';
      default:
        return null;
    }
  }
}
