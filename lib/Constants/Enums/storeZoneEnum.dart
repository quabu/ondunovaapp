enum StoreZoneType { Zone1, Zone2, Zone3, Zone4, Zone5, Zone6 }

extension StoreZoneTypeExtension on StoreZoneType {
  String get name {
    switch (this) {
      case StoreZoneType.Zone1:
        return 'Zona 1';
      case StoreZoneType.Zone2:
        return 'Zona 2';
      case StoreZoneType.Zone3:
        return 'Zona 3';
      case StoreZoneType.Zone4:
        return 'Zona 4';
      case StoreZoneType.Zone5:
        return 'Zona 5';
      case StoreZoneType.Zone6:
        return 'Zona 6';
      default:
        return null;
    }
  }
}
