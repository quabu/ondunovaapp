import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Navigation/Navigators/deliveredNavigator.dart';
//Navigators
import 'package:ondunova_app/Navigation/Navigators/depositNavigator.dart';
import 'package:ondunova_app/Navigation/Navigators/homeNavigator.dart';
import 'package:ondunova_app/Navigation/Navigators/pickupNavigator.dart';
import 'package:ondunova_app/Navigation/Navigators/storeNavigator.dart';
import 'package:ondunova_app/utils/materialColorFromHex.dart';
//Navigation
import 'botttomNavigationBarRootItem.dart';

class BottomNavigationBarController extends StatefulWidget {
  final int initialIndex;
  const BottomNavigationBarController({Key key, this.initialIndex})
      : super(key: key);

  @override
  BottomNavigationBarControllerState createState() =>
      BottomNavigationBarControllerState();
}

class BottomNavigationBarControllerState
    extends State<BottomNavigationBarController>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;
  List<int> _history = [0];
  GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();
  TabController _tabController;
  List<Widget> mainTabs;
  List<BuildContext> navStack = [null, null, null, null, null, null];
  Order returnedOrder;
  bool isUpdate = false;

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 5);
    mainTabs = <Widget>[
      Navigator(onGenerateRoute: (RouteSettings settings) {
        return PageRouteBuilder(pageBuilder: (context, animiX, animiY) {
          navStack[0] = context;
          return HomeNavigator(
            navigatorKey: GlobalKey<NavigatorState>(),
            changeTab: (int index) {
              changeMyTab(index);
            },
          );
        });
      }),
      Navigator(onGenerateRoute: (RouteSettings settings) {
        return PageRouteBuilder(pageBuilder: (context, animiX, animiY) {
          navStack[1] = context;
          return PickupNavigator(
            navigatorKey: GlobalKey<NavigatorState>(),
            order: returnedOrder,
            changeTab: (int index) {
              changeMyTab(index);
            },
            isUpdate: isUpdate,
          );
        });
      }),
      Navigator(onGenerateRoute: (RouteSettings settings) {
        return PageRouteBuilder(pageBuilder: (context, animiX, animiY) {
          navStack[2] = context;
          return DepositNavigator(
            navigatorKey: GlobalKey<NavigatorState>(),
            changeTab: (int index) {
              changeMyTab(index);
            },
          );
          /* return DepositScreen(); */
        });
      }),
      Navigator(onGenerateRoute: (RouteSettings settings) {
        return PageRouteBuilder(pageBuilder: (context, animiX, animiY) {
          navStack[3] = context;
          return DeliveredNavigator(
            navigatorKey: GlobalKey<NavigatorState>(),
            returnPalet: (int index, Order order) {
              returnOrder(index, order);
            },
            changeTab: (int index) {
              changeMyTab(index);
            },
          );
        });
      }),
      Navigator(onGenerateRoute: (RouteSettings settings) {
        return PageRouteBuilder(pageBuilder: (context, animiX, animiY) {
          navStack[4] = context;
          return StoreNavigator(
            navigatorKey: GlobalKey<NavigatorState>(),
            returnPalet: (int index, Order order) {
              returnOrder(index, order);
            },
            changeTab: (int index) {
              changeMyTab(index);
            },
          );
        });
      }),
    ];
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  final List<BottomNavigationBarRootItem> bottomNavigationBarRootItems = [
    BottomNavigationBarRootItem(
      bottomNavigationBarItem: BottomNavigationBarItem(
        icon: Icon(Icons.home),
        label: 'Home',
        backgroundColor: createMaterialColor(Color(0xFF00644b)),
      ),
      nestedNavigator: HomeNavigator(
        navigatorKey: GlobalKey<NavigatorState>(),
        changeTab: null,
      ),
      routeName: '/',
    ),
    BottomNavigationBarRootItem(
      bottomNavigationBarItem: BottomNavigationBarItem(
        icon: Icon(Icons.archive),
        label: 'Entrada Magatzem',
        backgroundColor: createMaterialColor(Color(0xFF00644b)),
      ),
      nestedNavigator: PickupNavigator(
        navigatorKey: GlobalKey<NavigatorState>(),
        changeTab: null,
        isUpdate: false,
      ),
      routeName: '/',
    ),
    BottomNavigationBarRootItem(
      bottomNavigationBarItem: BottomNavigationBarItem(
        icon: Icon(Icons.unarchive),
        label: 'Portar a Màquina',
        backgroundColor: createMaterialColor(Color(0xFF00644b)),
      ),
      nestedNavigator: DepositNavigator(
        navigatorKey: GlobalKey<NavigatorState>(),
        changeTab: null,
      ),
      routeName: '/',
    ),
    BottomNavigationBarRootItem(
      bottomNavigationBarItem: BottomNavigationBarItem(
        icon: Icon(Icons.assignment_return),
        label: 'Devolució / Retirada',
        backgroundColor: createMaterialColor(Color(0xFF00644b)),
      ),
      nestedNavigator: DeliveredNavigator(
        navigatorKey: GlobalKey<NavigatorState>(),
        changeTab: null,
      ),
      routeName: '/',
    ),
    BottomNavigationBarRootItem(
      bottomNavigationBarItem: BottomNavigationBarItem(
        icon: Icon(Icons.storage),
        label: 'Gestió Magatzem',
        backgroundColor: createMaterialColor(Color(0xFF00644b)),
      ),
      nestedNavigator: StoreNavigator(
        navigatorKey: GlobalKey<NavigatorState>(),
        returnPalet: null,
        changeTab: null,
      ),
      routeName: '/',
    ),
  ];

  changeMyTab(int index) {
    _tabController.index = index;
    setState(() {
      isUpdate = false;
      _selectedIndex = index;
      _history.add(index);
      _navigatorKey.currentState;
    });
  }

  returnOrder(int index, Order order) {
    _tabController.index = index;
    setState(() {
      isUpdate = true;
      returnedOrder = order;
      _selectedIndex = index;
      _history.add(index);
      _navigatorKey.currentState;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: TabBarView(
            controller: _tabController,
            physics: NeverScrollableScrollPhysics(),
            children: mainTabs),
        bottomNavigationBar: BottomNavigationBar(
          items: bottomNavigationBarRootItems
              .map((e) => e.bottomNavigationBarItem)
              .toList(),
          showUnselectedLabels: true,
          unselectedItemColor: Color(0xFFe6e6e6),
          currentIndex: _selectedIndex,
          selectedItemColor: createMaterialColor(Color(0xFFb7ce00)),
          onTap: _onItemTapped,
        ),
      ),
      onWillPop: () async {
        if (Navigator.of(navStack[_tabController.index]).canPop()) {
          Navigator.of(navStack[_tabController.index]).pop();
          setState(() {
            _selectedIndex = _tabController.index;
          });
          return false;
        } else {
          if (_tabController.index == 0) {
            setState(() {
              _selectedIndex = _tabController.index;
            });
            SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            return true;
          } else {
            _tabController.index = 0;
            setState(() {
              _selectedIndex = _tabController.index;
            });
            return false;
          }
        }
      },
    );
  }

  void _onItemTapped(int index) {
    _tabController.index = index;
    setState(() {
      isUpdate = false;
      returnedOrder = null;
      _selectedIndex = index;
      _history.add(index);
      _navigatorKey.currentState;
    });
  }
}
