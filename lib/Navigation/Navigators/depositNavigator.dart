import 'package:flutter/material.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';
//Navigation
import '../tabBar/nestedNavigator.dart';
//Screens
import 'package:ondunova_app/Screens/DepositScreens/depositScreen.dart';
import 'package:ondunova_app/Screens/DepositScreens/detailsOfDepositScreen.dart';

class DepositNavigator extends NestedNavigator {
  final void Function(int) changeTab;
  final OrderJob orderJob;
  DepositNavigator(
      {Key key,
      @required GlobalKey<NavigatorState> navigatorKey,
      @required this.changeTab,
      this.orderJob})
      : super(
          key: key,
          navigatorKey: navigatorKey,
        );

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case '/':
            builder =
                (BuildContext context) => DepositScreen(changeTab: changeTab);
            break;
          case '/detailOrderJob':
            builder = (BuildContext context) => DetailsOfDepositScreen(
                  changeTab: changeTab,
                  orderJob: orderJob,
                );
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(
          builder: builder,
          settings: settings,
        );
      },
    );
  }
}
