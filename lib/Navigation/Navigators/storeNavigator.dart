import 'package:flutter/material.dart';
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Screens/StoreScreens/storeZoneScreen.dart';
import '../tabBar/nestedNavigator.dart';
//Screens
import 'package:ondunova_app/Screens/StoreScreens/storeScreen.dart';

class StoreNavigator extends NestedNavigator {
  final Order order;
  final void Function(int, Order) returnPalet;
  final void Function(int) changeTab;
  StoreNavigator({
    Key key,
    @required GlobalKey<NavigatorState> navigatorKey,
    this.order,
    @required this.returnPalet,
    @required this.changeTab,
  }) : super(
          key: key,
          navigatorKey: navigatorKey,
        );

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case '/':
            builder = (BuildContext context) =>
                StoreScreen(returnPalet: returnPalet, changeTab: changeTab);
            break;
          case '/storeZone':
            builder = (BuildContext context) =>
                StoreZoneScreen(order: order, changeTab: changeTab);
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(
          builder: builder,
          settings: settings,
        );
      },
    );
  }
}
