import 'package:flutter/material.dart';
import '../tabBar/nestedNavigator.dart';
//Screens
import 'package:ondunova_app/Screens/homeScreen.dart';

class HomeNavigator extends NestedNavigator {
  final void Function(int) changeTab;
  HomeNavigator(
      {Key key,
      @required GlobalKey<NavigatorState> navigatorKey,
      @required this.changeTab})
      : super(
          key: key,
          navigatorKey: navigatorKey,
        );

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case '/':
            builder =
                (BuildContext context) => HomeScreen(changeTab: changeTab);
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(
          builder: builder,
          settings: settings,
        );
      },
    );
  }
}
