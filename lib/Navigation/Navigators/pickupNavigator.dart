import 'package:flutter/material.dart';
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Screens/PickupScreens/zonePickupScreen.dart';
import 'package:ondunova_app/Screens/PickupScreens/autoPickupScreen.dart';
import '../tabBar/nestedNavigator.dart';
//Screens
import 'package:ondunova_app/Screens/PickupScreens/pickupScreen.dart';

class PickupNavigator extends NestedNavigator {
  final Order order;
  final void Function(int) changeTab;
  final bool isUpdate;
  final bool isError;
  PickupNavigator(
      {Key key,
      @required GlobalKey<NavigatorState> navigatorKey,
      this.order,
      @required this.changeTab,
      this.isUpdate,
      this.isError})
      : super(
          key: key,
          navigatorKey: navigatorKey,
        );

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case '/':
            builder = (BuildContext context) => AutoPickupScreen(
                order: order, changeTab: changeTab, isUpdate: isUpdate);
            break;
          case '/pickupScreen':
            builder = (BuildContext context) => PickupScreen(
                order: order, changeTab: changeTab, isUpdate: isUpdate);
            break;
          case '/zonePickupScreen':
            builder = (BuildContext context) => ZonePickupScreen();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(
          builder: builder,
          settings: settings,
        );
      },
    );
  }
}
