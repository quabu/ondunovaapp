import 'package:flutter/material.dart';
//Class
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';
import 'package:ondunova_app/Screens/DeliveredScreens/detailsOfDeliveredScreen.dart';
import 'package:ondunova_app/Screens/DeliveredScreens/storeZoneDeliveredScreen.dart';
//Navigation
import '../tabBar/nestedNavigator.dart';
//Screens
import 'package:ondunova_app/Screens/DeliveredScreens/deliveredScreen.dart';

class DeliveredNavigator extends NestedNavigator {
  final void Function(int, Order) returnPalet;
  final void Function(int) changeTab;
  final OrderJob orderJob;
  final Order order;

  DeliveredNavigator(
      {Key key,
      @required GlobalKey<NavigatorState> navigatorKey,
      @required this.changeTab,
      this.returnPalet,
      this.orderJob,
      this.order})
      : super(
          key: key,
          navigatorKey: navigatorKey,
        );

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case '/':
            builder = (BuildContext context) =>
                DeliveredScreen(returnPalet: returnPalet, changeTab: changeTab);
            break;
          case '/detailOrderJob':
            builder = (BuildContext context) => DetailsOfDeliveredScreen(
                  changeTab: changeTab,
                  orderJob: orderJob,
                );
            break;
          case '/zoneChange':
            builder = (BuildContext context) => StoreZoneDeliveredScreen(
                  changeTab: changeTab,
                  order: order,
                );
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(
          builder: builder,
          settings: settings,
        );
      },
    );
  }
}
