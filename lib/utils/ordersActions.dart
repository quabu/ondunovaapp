import 'package:flutter/foundation.dart';
import "package:collection/collection.dart";
import 'package:ondunova_app/Api/apiProvider.dart';
import 'package:ondunova_app/Class/apiResponseClass.dart';
import 'package:ondunova_app/Class/groupedOrderClass.dart';
//Class
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';
//Enums
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';
import 'package:ondunova_app/Constants/storeFilterTypes.dart';

class OrderActions {
  //getOrders
  List<Order> getOrdersFromResponse(ApiResponse apiResponse) {
    Iterable l = apiResponse.data;
    List<Order> orders =
        List<Order>.from(l.map((model) => Order.fromJson(model)));
    return orders;
  }

  //Filter by status
  List<Order> filterByOrderStatus(List<Order> orders, OrderStatus orderStatus) {
    List<Order> filteredOrders = orders
        .where((element) => element.orderStatus == describeEnum(orderStatus))
        .toList();
    return filteredOrders;
  }

  //Filter by status and OF
  List<Order> filterByOrderStatusAndOf(Order order, List<Order> orders) {
    List<Order> filteredOrders = orders
        .where((element) =>
            element.orderStatus == order.orderStatus &&
            element.numOf == order.numOf)
        .toList();
    return filteredOrders;
  }

  // ** PickupScrenn **
  //setValidatedOrder
  Order getValidateOrderFromResponse(ApiResponse apiResponse) {
    if (apiResponse.data != null) {
      Iterable l = apiResponse.data;
      List<OrderJob> orderJobs =
          List<OrderJob>.from(l.map((model) => OrderJob.fromJson(model)));
      OrderJob orderJob = orderJobs[0];
      Order order = new Order(
          numOf: orderJob.orderNumber + orderJob.jobNumber,
          totalPalets: orderJob.noPallets,
          machineId: orderJob.machineCode,
          machineName: orderJob.machineName,
          clientId: orderJob.clientId,
          clientName: orderJob.clientName,
          origin: "Onduladora");
      return order;
    } else {
      return null;
    }
  }

  // ** StoreScreen **
  //Get grouped orders to show grouped list
  getStoreOrdersAndGroupedOrders(List<Order> orderList, String choice) {
    List<GroupedOrders> groupedOrders;
    List<Order> orders =
        orderList.where((element) => element.storePosition != null).toList();

    if (choice == StoreFilterTypes.ByNumOF) {
      orders.sort((a, b) => a.numPalet.compareTo(b.numPalet));
      groupedOrders = groupStoreOrderByChoice(orders, choice);
    } else {
      orders.sort((a, b) => a.numOf.compareTo(b.numOf));
      groupedOrders = groupStoreOrderByChoice(orders, choice);
    }

    groupedOrders.sort((a, b) => a.group.compareTo(b.group));

    return [orders, groupedOrders];
  }

  //Group store orders by choice
  groupStoreOrderByChoice(List<Order> orderList, String choice) {
    var mapedOrders;
    List<GroupedOrders> groupedOrders = [];
    if (choice == StoreFilterTypes.ByNumOF) {
      mapedOrders = groupBy(orderList, (Order order) => order.numOf);
    } else {
      mapedOrders = groupBy(orderList, (Order order) => order.storePosition);
    }

    mapedOrders.forEach((k, v) {
      groupedOrders.add(GroupedOrders(k, v));
    });

    return groupedOrders;
  }
}
