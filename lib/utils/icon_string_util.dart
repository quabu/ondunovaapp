import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'Icons.archive': Icons.archive,
  'Icons.unarchive': Icons.unarchive,
  'Icons.assignment_return': Icons.assignment_return,
  'Icons.storage': Icons.storage,
  'Icons.info': Icons.info
};

Icon getIcon(String nombreIcono) {
  return Icon(
    _icons[nombreIcono],
    color: Color(0xFFb7ce00),
    size: 30,
  );
}
