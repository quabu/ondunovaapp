import 'package:flutter/foundation.dart';
import "package:collection/collection.dart";
import 'package:ondunova_app/Api/apiProvider.dart';
import 'package:ondunova_app/Class/apiResponseClass.dart';
import 'package:ondunova_app/Class/groupedOrderClass.dart';
//Class
import 'package:ondunova_app/Class/orderClass.dart';
import 'package:ondunova_app/Class/orderJobClass.dart';
//Enums
import 'package:ondunova_app/Constants/Enums/orderStatusEnum.dart';
import 'package:ondunova_app/Constants/storeFilterTypes.dart';

class OrderJobsActions {
  //getOrders
  List<OrderJob> getOrdersJobsFromResponse(ApiResponse apiResponse) {
    Iterable l = apiResponse.data;
    List<OrderJob> orderJobs =
        List<OrderJob>.from(l.map((model) => OrderJob.fromJson(model)));
    return orderJobs;
  }

  //Merge Orders to OrderJobs
  List<OrderJob> mergeOrdersToOrderJobs(
      List<OrderJob> orderJobs, List<Order> orders, OrderStatus orderStatus) {
    if (orders.length > 0) {
      orders.sort((a, b) => a.numPalet.compareTo(b.numPalet));
      var orderIndex = 0;
      for (final orderJob in orderJobs) {
        for (final order in orders) {
          if (order.orderStatus == describeEnum(orderStatus) &&
              order.numOf.trim() == orderJob.orderNumber.trim()) {
            orderJob.orders.insert(orderIndex, order);
            orderIndex++;
          }
        }
        orderIndex = 0;
      }
      return orderJobs;
    } else {
      return orderJobs;
    }
  }
}
