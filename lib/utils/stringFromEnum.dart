import 'package:ondunova_app/Constants/Enums/storeZoneEnum.dart';

class StringFromEnum {
  //StoreZoneType
  getStoreZoneValue(String stringZone) {
    StoreZoneType zoneType = StoreZoneType.Zone1;
    switch (stringZone) {
      case 'Zona 1':
        zoneType = StoreZoneType.Zone1;
        break;
      case 'Zona 2':
        zoneType = StoreZoneType.Zone2;
        break;
      case 'Zona 3':
        zoneType = StoreZoneType.Zone3;
        break;
      case 'Zona 4':
        zoneType = StoreZoneType.Zone4;
        break;
      case 'Zona 5':
        zoneType = StoreZoneType.Zone5;
        break;
      case 'Zona 6':
        zoneType = StoreZoneType.Zone6;
        break;
    }

    return zoneType;
  }

  getStringFromZoneType(zoneType) {
    var zoneString;
    switch (zoneType) {
      case StoreZoneType.Zone1:
        zoneString = 'Zona 1';
        break;
      case StoreZoneType.Zone2:
        zoneString = 'Zona 2';
        break;
      case StoreZoneType.Zone3:
        zoneString = 'Zona 3';
        break;
      case StoreZoneType.Zone4:
        zoneString = 'Zona 4';
        break;
      case StoreZoneType.Zone5:
        zoneString = 'Zona 5';
        break;
      case StoreZoneType.Zone6:
        zoneString = 'Zona 6';
        break;
    }

    return zoneString;
  }
}
