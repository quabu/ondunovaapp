# README #

### What is this repository for? ###

App para gestionar el alamacen intermedio de Ondunova

Version: 2.2

### How do I get set up? ###
# Install and update plugins
flutter pub get
flutter pub outdated
flutter pub upgrade

# Run app
flutter run

# Build app apk
flutter build apk

# Utils
flutter analyze
flutter test
flutter run

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Bastien Orieux